import styles from '../../styles/components/Patterns/MiniCard.module.css'

export default function MiniCard({ icon, title, text }) {
  return(
    <div className={styles.card}>
      <div>
        <img src={icon} />
      </div>
      <h2>{title}</h2>
      <p>{text}</p>
    </div>
  );
}