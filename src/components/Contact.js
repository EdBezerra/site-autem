import styles from '../styles/components/Contact.module.css';
import SectionLabel from '../components/Patterns/SectionLabel';

import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import axios from 'axios';
import { useState } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


export default function Contact() {

  const notifySuccess = () => {
    toast.success("🚀 Email enviado, agradecemos o contato!", {
      position: toast.POSITION.TOP_RIGHT,
      pauseOnHover: true
    });
  }

  const notifyError = () => {
    toast.error("😥 Ops, não conseguimos completar a sua solicitação...", {
      position: toast.POSITION.TOP_RIGHT,
      pauseOnHover: true
    });
  }

  const notifyInfo = () => {
    toast.info("😎 Aguarde, sua mensagem está sendo enviada!", {
      position: toast.POSITION.TOP_RIGHT,
      pauseOnHover: true
    });
  }

  const schema = yup.object().shape({
    name: yup.string().required('Nome é obrigatório.').min(3, 'Digite um nome válido.'),
    email: yup.string().email('Digite um e-mail válido.').required('E-mail é obrigatório.'),
    phone: yup.string().max(11, 'Digite um telefone válido').required('Telefone é obrigatório.')
  })

  const {
    register,
    handleSubmit,
    reset,
    formState:{
      errors,
    } 
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: { something: "anything" }
  });

  const [submittedData, setSubmittedData] = useState({});

  const onSubmit = (data) => {
    notifyInfo();
    setSubmittedData(data);
    axios({
      method: 'post',
      url: 'http://localhost:8080/contact',
      data: {
        name: data.name,
        email: data.email,
        phone: data.phone,
        msg: data.message
      },
    })
    .then(function (response) {
      notifySuccess();
      reset({ ...submittedData });
    })
    .catch(function (error) {
      notifyError();
    });
  };

  function validate(evt) {
    var theEvent = evt || window.event;
  
    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
    // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }


  return(
    <div className={styles.contact} id="contact">
      <div data-aos="fade-up" className={styles.container}>
        <section>
          <div className={styles.contactInfo}>
            <SectionLabel
              text="Contato"
            />
            <h2>
              Contate-nos
            </h2>
            <p>
            Estamos aqui para te ajudar a ter a melhor experiência de gestão.
             Tire dúvidas sobre nossos produtos, serviços, website e mais.
            </p>

            <div>
              <div className={styles.contactLinkContainer}>
                <div className={styles.icon}>
                  <img src="/icons/phone.svg"/>
                </div>
                <div className={styles.contactLink}>
                  <span>
                    Telefone
                  </span>
                  <a href="tel:551142300152">11 4230-0152</a>
                </div>
              </div>

              <div className={styles.contactLinkContainer}>
                <div className={styles.icon}>
                  <img src="/icons/mail.svg"/>
                </div>
                <div className={styles.contactLink}>
                  <span>
                    E-mail
                  </span>
                  <a href="mailto:faleconosco@satellitus.com">faleconosco@satellitus.com</a>
                </div>
              </div>

              <div className={styles.contactLinkContainer}>
                <div className={styles.icon}>
                  <img src="/icons/map.svg"/>
                </div>
                <div className={styles.contactLink}>
                  <span>
                    Endereço
                  </span>
                  <a target="__blank" href="https://g.page/satellitustecnologia?share">Av. Dona Manoela Larcerda de Vergueiro,
                                                                                        58, Anhangabaú, Jundiaí - SP</a>
                </div>
              </div>
            </div>
          </div>

          <div>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className={styles.formInput}>
                <label>Nome</label>
                <input
                  type="text"
                  placeholder="Seu nome completo aqui"
                  name="name"
                  {...register("name")}
                  />
                  <p className={styles.error}>{errors.name?.message}</p>
              </div>

              <div className={styles.formInput}>
                <label>E-mail</label>
                <input
                  type="email"
                  placeholder="Seu e-mail para contato"
                  name="email"
                  {...register('email')}
                />
                <p className={styles.error}>{errors.email?.message}</p>
              </div>

              <div className={styles.formInput}>
                <label>Celular</label>
                <input
                  onKeyPress={validate}
                  type="tel"
                  placeholder="Seu celular para contato"
                  name="phone"
                  maxLength="11"
                  minLength="11"
                  {...register('phone')}
                />
                <p className={styles.error}>{errors.phone?.message}</p>
              </div>

              <div className={styles.formInput}>
               <label>Mensagem</label>
                <textarea
                  placeholder="Sua mensagem aqui"
                  name="message"
                  {...register('message')}
                />
              </div>

              <div className={styles.buttonContainer}>
                <button
                  className={styles.sendButton}
                  type="submit"
                  aria-label="Enviar Formulário"
                >
                  Enviar mensagem
                </button>
              </div>
            </form>
          </div>
        </section>
      </div>
      <ToastContainer />
    </div>
  );
}