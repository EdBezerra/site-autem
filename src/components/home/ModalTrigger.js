import styles from '../../styles/components/home/ModalTrigger.module.css';
import { useContext, useState } from "react";
import { ModalTriggerContext } from '../../contexts/ModalTriggerContext';
import ReactTooltip from 'react-tooltip';

export default function ModalTrial() {
  const { modalStatus, closeModalTrigger, goSat } = useContext(ModalTriggerContext);

  return(
    <div className={modalStatus == false ? styles.overlay : styles.overlayTwo}>
      <div className={modalStatus == false ? styles.container : styles.containerAlt}>
        <section>
          <div className={styles.triggerHeader}>
          <button
            className={styles.closeButton}
            type="button"
            onClick={closeModalTrigger}
          >
            <img src="/icons/close.svg" alt="Fechar modal" />
          </button>
            <img src="icons/location-arrow-white.svg" />
            Acionamento
          </div>
          <div className={styles.triggerBody}>
            <div>
              <img className={styles.userIcon} src="/images/sat.jpg"/>
            </div>
            <div>
              <span>Sat</span>
              <span className={styles.km}>7km</span>
              <div data-tip data-for='signal' className={styles.signal}>
                <img src="/icons/signal.png" />
              </div>
              <ReactTooltip id="signal" type="warning" backgroundColor="#FF9945">
                Sinal fraco.
              </ReactTooltip>
              <div data-tip data-for='disponibilty' className={styles.disponibilty}>
                <img src="/icons/wrench.png" />
              </div>
              <ReactTooltip id="disponibilty">
                Profissional livre
              </ReactTooltip>
              <div data-tip data-for='working' className={styles.working}>
                <img src="/icons/bolt.png" />
              </div>
              <ReactTooltip id="working">
                Em expediente
              </ReactTooltip>
              <span data-tip data-for='services' className={styles.services}>
                18
              </span>
              <ReactTooltip id="services">
                18 serviços nas últimas 24 horas.
              </ReactTooltip>
            </div>
            <div className={styles.triggerButton}>
              <button onClick={goSat}>
                Acionar!
              </button>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}