import Head from 'next/head';
import Customer from "../components/home/Customer";
import Features from "../components/home/Features";
import Homepage from "../components/home/Homepage";
import Mobile from '../components/home/Mobile';
import Report from "../components/home/Report";
import System from "../components/home/System";
import Testimonial from "../components/home/Testimonial";
import PostsBlog from "../components/home/PostsBlog";
import Trial from "../components/home/Trial";

import { ModalProvider } from '../contexts/ModalTrialContext';
import { ModalTriggerProvider } from '../contexts/ModalTriggerContext';


export default function Home({posts}) {
  
  return (
    <ModalProvider>
      <ModalTriggerProvider>
        <Head>
          <title>AutEM | Gestão Inteligente</title>
          <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        </Head>
        <Homepage/>
        <Customer/>
        <System />
        <Report />
        <Features />
        <Testimonial />
        <Trial />
        <PostsBlog data={posts}  />
      </ModalTriggerProvider>
    </ModalProvider>
  )
}

export async  function getServerSideProps() {
  const res = await fetch(process.env.URI_HOST+'/api/posts');
   
  const posts = await res.json();
  // console.log(posts);
  return {
    props: {
      posts
    }
  }
}

