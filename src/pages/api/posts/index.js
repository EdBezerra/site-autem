import excuteQuery from '../../../../lib/db';

export default async (req, res) => {
  const queryPosts = `SELECT
	ID ,
	guid as post_url,
	post_title,
	post_content,
	post_date_gmt
FROM
	wp_posts
LEFT JOIN wp_term_relationships tr ON
	tr.object_id = ID 
	AND tr.term_taxonomy_id IN (3, 10)
LEFT JOIN wp_terms wt ON
	wt.term_id = tr.term_taxonomy_id
WHERE
	post_type = 'post'
AND 
	wt.term_id IN (3, 10)
AND 
	post_status IN ('publish', 'inherit')
GROUP BY
	ID 
ORDER BY
	ID 
DESC
LIMIT 4
`;

  const queryImg = `SELECT
post_parent ,
guid AS img_src
FROM
wp_posts
WHERE
post_mime_type = 'image/png'
AND
post_parent <> 0
GROUP BY
post_parent
ORDER BY
post_parent
DESC
LIMIT 12
`;

  try {
    const data = [];

    switch (req.method) {
      case 'GET':
        const resultPosts = await excuteQuery({
          query: queryPosts,
        });

        const resultImg = await excuteQuery({
          query: queryImg,
        });

        let url = 'Sem Imagen registrada';
        if (resultPosts.length > 0 && resultImg.length > 0) {
          for (let i = 0; i < resultPosts.length; i++) {
            const img = function () {
              for (let j = 0; j < resultImg.length; j++) {
                if (resultImg[j].post_parent === resultPosts[i].ID && resultImg[j].img_src !== null && resultImg[j].img_src !== '') {
                  url = resultImg[j].img_src;
                 // console.log(resultPosts[i].ID + ' - ' +resultImg[j].post_parent);
                  break;
                }
              }
            }();

            /* 
            ID 
            post_url,
            post_title,
            post_content,
            post_date_gmt,
            post_parent- validação
            img_src 
            */

            data.push({ 
              id: resultPosts[i].ID,
              post_url: resultPosts[i].post_url,
              post_title: resultPosts[i].post_title,
              post_content: content_clear(resultPosts[i].post_content),
              post_date: format_dt_public(resultPosts[i].post_date_gmt),
              post_img: url
            });

          }//FOR01
        }//IF01

      res.status(200).json(data);
      break;

      default:
      res.status(405).json({ message: `Method ${req.method} Not Permited` });
      break;

    }// SWITCH
  } catch (error) {
    res.status(500).json({ statusCode: 500, message: error.message });
  }


};

function content_clear(texto) {
  //Remover tags html e resumir em 200 caracteress
  return texto.replace(/<(?:.|\n)*?>/gm, ' ').substring(0, 200);
}

function format_dt_public(data) {
  const dt = new Date(data);
  const dia = (dt.getDate() < 10 ? '0' : '') + dt.getDate();
  const mes = format_month(dt.getMonth() + 1);
  const ano = dt.getFullYear();
  return `${dia} ${mes} ${ano}`;
}

function format_month(data) {
  var mes = new Array();
  mes[0] = "Jan";
  mes[1] = "Fev";
  mes[2] = "Mar";
  mes[3] = "Abr";
  mes[4] = "Mai";
  mes[5] = "Jun";
  mes[6] = "Jul";
  mes[7] = "Aug";
  mes[8] = "Set";
  mes[9] = "Out";
  mes[10] = "Nov";
  mes[11] = "Dez";
  return  mes[data];
}