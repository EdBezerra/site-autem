import Header from '../components/Header';
import Contact from '../components/Contact';
import Footer from '../components/Footer';
import '../styles/globals.css';

// import { IntercomProvider, useIntercom } from 'react-use-intercom';

import AOS from 'aos';
import 'aos/dist/aos.css';
import { useEffect } from 'react';

function MyApp({ Component, pageProps }) {
  // const INTERCOM_APP_ID = 'n2lk4hf3';
  
  useEffect(() => {
    AOS.init({ duration : 1500});
  }, []);
  
  return(
    <>
      <>
        <Header />
        <Component {...pageProps} />
        <Contact />
        <Footer />
      </>
    </>
  );
}

export default MyApp
