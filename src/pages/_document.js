import Document, { Html, Head, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="pt-br">
        <Head>
          <meta name="title" content="AutEM | Gestão Inteligente | Gestão de Equipes Externas" />
          <meta name="keywords"
            content="Gerenciamento de chamados, Software, Ordem de serviço, Chamados técnicos,
                     Sistema de chamados, Sistema para assistência técnica, Sistema para assistências
                     24 horas, Controle de chamados, Gestão de ordem de serviço, Ordem de serviço"
          />
          <meta name="description" content="AutEM, gestão de equipes externas. Proporcionando toda a gestão do seu negócio na palma da mão, desde os controles básicos logísticos aos avançados relatórios gerenciais" />
          <meta name="image" content="" />

          <meta property="og:type" content="website" />
          <meta property="og:url" content="" />
          <meta property="og:title" content="AutEM | Gestão Inteligente | Gestão de Equipes Externas" />
          <meta property="og:description" content="AutEM, gestão de equipes externas. Proporcionando toda a gestão do seu negócio na palma da mão, desde os controles básicos logísticos aos avançados relatórios gerenciais" />
          <meta property="og:image" content="banner.png" />

          <meta property="twitter:card" content="summary_large_image" />
          <meta property="twitter:url" content="" />
          <meta property="twitter:title" content="AutEM | Gestão Inteligente | Gestão de Equipes Externas" />
          <meta property="twitter:description" content="AutEM, gestão de equipes externas. Proporcionando toda a gestão do seu negócio na palma da mão, desde os controles básicos logísticos aos avançados relatórios gerenciais" />
          <meta property="twitter:image" content="banner.png" />

          <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}