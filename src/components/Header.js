import { useState, useEffect } from 'react';
import styles from '../styles/components/Header.module.css';
import { Link } from 'react-scroll';

export default function Header() {
  const [isActive, setIsActive] = useState(false);
  const [path, setPath] = useState();

  const handleMenu = () => setIsActive(!isActive);

  function closeMenu() {
    setIsActive(false);
  }

  useEffect(() => {
    setPath(window.location.pathname)
  })

  return(
    <nav className={styles.navbar}>
      <div>
        <a href="/">
          <img src="/images/logo/logo.svg" alt="Logo AutEM"/>
        </a>
      </div>

      <ul
        className={(isActive == true ?  `${styles.navigation} ${styles.nav_active} ` : `${styles.navigation}`)}
      >
        {
          path == '/terms' ? (
            <>
              <li>
                <a href="/">
                  Home
                </a>
              </li>
              <li>
                <a href="/">
                  AutEM
                </a>
              </li>
              <li>
                <a href="/">
                  Soluções
                </a>
              </li>
              <li>
                <Link to="contact" smooth={true} duration={1000}>
                  Contato
                </Link>
              </li>
              <li>
                <a href="https://blog.satellitus.com/" target="__blank">
                  Blog
                </a>
              </li>
            </>
          ) : (
            <>
              <li>
                <Link to="home" smooth={true} duration={1000}>
                  Home
                </Link>
              </li>
              <li>
                <Link to="autem" smooth={true} duration={1000}>
                  AutEM
                </Link>
              </li>
              <li>
                <Link to="report" smooth={true} duration={1000}>
                  Soluções
                </Link>
              </li>
              <li>
                <Link to="contact" smooth={true} duration={1000}>
                  Contato
                </Link>
              </li>
              <li>
                <a href="https://blog.satellitus.com/" target="__blank">
                  Blog
                </a>
              </li>
            </>
          )
        }
      </ul>

      <div>
        <a
          className={styles.loginButton}
          href="https://web.autem.com.br/login/"
          target="__blank"
        >
          <img src="/icons/login.svg" />
          Login
        </a>
      </div>

      <button
        type="button"
        className={(isActive == true ? `${styles.burger} ${styles.toggle}` : `${styles.burger}`)}
        onClick={handleMenu}
        aria-label="Menu de Navegação"
      >
        <div className={styles.line1}></div>
        <div className={styles.line2}></div>
        <div className={styles.line3}></div>
      </button>
    </nav>
  );
}