import { createContext, useContext, useState } from 'react';
import { ModalTrial } from '../components/ModalTrial';

export const ModalContext = createContext({});

export function ModalProvider({ children, ...rest }) {

  const [isModal, setIsModal] = useState(false);
  
  function openModal() {
    setIsModal(true);
  }
  
  function closeModal() {
    setIsModal(false);
  }

  return (
    <ModalContext.Provider
      value={{
        isModal,
        setIsModal,
        openModal,
        closeModal
      }}
    >
      {children}
      { isModal && <ModalTrial /> }
    </ModalContext.Provider>
  )
}
