import styles from '../../styles/components/Patterns/Button.module.css';

export default function Button({text, link, action, type }) {
  return(
    <div className={styles.button}>
      <a
        href={link}
        type={type }
        onClick={action}
      >
        {text}
      </a>
    </div>
  );
}
