import styles from '../../styles/components/home/System.module.css';
import SectionLabel from '../Patterns/SectionLabel';
import { Link } from 'react-scroll';

export default function System() {
  return(
    <div className={styles.system}>
      <div className={styles.container} id="autem">
        <section>
          <div data-aos="fade-right">
            <img className={styles.systemDashboard} src="/images/dashboard.png" alt="Dashboard AutEM" />
            <img className={styles.dashCard} src="/images/dashCard.png" />
            <img className={styles.dashCardAlt} src="/images/dashCardAlt.png" />
          </div>
          <div data-aos="fade-left" className={styles.systemInfo}>
            <SectionLabel
              text="SISTEMA"
            />
            <h2>
            A evolução que você precisa
            </h2>
            <p>
            Com o alto volume de informações, é necessário filtrar 
            dados para tomar a melhor decisão. O Dashboard é a 
            ferramenta ideal para análise de produtividade do dia
             a dia da sua empresa.
            </p>
            <div className={styles.button}>
              <Link to="report" smooth={true} duration={1000}>
                Saiba mais
              </Link>
            </div>
          </div>
        </section>
        <img className={`${styles.element} + ${styles.elementOne}`}src="/elements/dotPattern.png" />
        <img className={`${styles.element} + ${styles.elementTwo}`} src="/elements/dotPatternAlt.png" />
        <img className={`${styles.element} + ${styles.elementThree}`} src="/elements/containerHero.png" />
      </div>
    </div>
  );
}