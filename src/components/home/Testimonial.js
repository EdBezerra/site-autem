import { useRef } from 'react';
import styles from '../../styles/components/home/Testimonial.module.css';
import SectionLabel from '../Patterns/SectionLabel';
import TestimonialCard from '../Patterns/TestimonialCard';

export default function Testimonial() {
  const scrollItems = useRef(null);

  function nextItem() {
    scrollItems.current.scrollBy(300, 0)
  }

  function previousItem() {
    scrollItems.current.scrollBy(-300, 0)
  }

  return(
    <div className={styles.testimonial}>
      <div className={styles.container}>
        <section>
          <div data-aos="fade-right" className={styles.testimonialInfo}>
            <SectionLabel
              text="FEEDBACK"
            />
            <h2>
              O que nossos clientes pensam
            </h2>
            <div className={styles.actionButtons}>
              <button
                className={styles.buttonBack}
                onClick={previousItem}
              >
                <img src="/icons/arrow-left.svg"/>
              </button>
              <button
                className={styles.buttonNext}
                onClick={nextItem}
              >
                <img src="/icons/arrow-right.svg"/>
              </button>
            </div>
          </div>
          <div data-aos="fade-left" className={styles.itensWrapper}>
            <div
              className={styles.items}
              ref={scrollItems}
            >
              <TestimonialCard
                image="/images/testimonials/Fabiano.png"
                
                alt="Depoimento Fabiano - Total Import"
                title="Fabiano"
                subtitle="Total Import"
                text="“Tenho somente elogios para o AutEM, uma ferramenta essencial e perfeita para minha empresa, pois com ela é possível ter tudo nas palmas das minhas mãos!”"
              />
              <TestimonialCard
                image="/images/testimonials/Fernando.png"
                alt="Depoimento Fernando - Flay Assistência"
                title="Fernando"
                subtitle="Flay Assistência"
                text="“==Gosto muito do AutEM. Ele colaborou com a minha empresa em 100%, principalmente na parte de acionamentos com os aplicativos!”"
              />
              <TestimonialCard
                image="/images/testimonials/Marcia.png"
                alt="Depoimento Marcia - Mecânica Sundyn"
                title="Marcia"
                subtitle="Mecânica Sundyn"
                text="“Antes de iniciar no AutEM, nossa empresa atuava com um sistema muito limitado e impreciso. No AutEM encontramos mais precisão e o sistema é bem amplo.”"
              />
              <TestimonialCard
                image="/images/testimonials/user.png"
                alt="Depoimento Maria Rosângela - Gonçalves Auto"
                link="https://blog.satellitus.com/index.php/2021/05/31/ha-seis-anos-com-o-autem-empresa-goncalves-auto-otimiza-gestao/"
                title="Maria"
                subtitle="Gonçalves Auto"
                text="“Antes o nosso TMC era de 60 minutos e hoje com o AutEM o nosso TMC está em uma média de 40 a 42 minutos. Claro que esse valor varia conforme a seguradora, mas a redução que tivemos é nítida.”"
              />
              <TestimonialCard
                image="/images/testimonials/user.png"
                alt="Depoimento Igor Alexandre Dias - Fast Service"
                link="https://blog.satellitus.com/index.php/2021/06/14/fast-service-quality-ve-no-autem-solucao-para-reduzir-custos-e-conquistar-mercado/"
                title="Igor"
                subtitle="Fast Service"
                text="“Antes do AutEM eu tinha três sistemas para conseguir gerenciar a minha empresa. Já com o AutEM consegui unificar todas essas atividades em apenas um sistema e isso ajudou muito a empresa.”"
              />
              <TestimonialCard
                image="/images/testimonials/user.png"
                alt="Depoimento Gilson Oliveira - Chavelândia"
                link="https://blog.satellitus.com/index.php/2021/07/20/chavelandia-digitaliza-processos-com-o-autem-e-transforma-gestao/"
                title="Gilson"
                subtitle="Chavelândia"
                text="“O nosso TMC melhorou consideravelmente com a implantação do sistema. Nós conseguimos cobrar de forma mais efetiva o técnico agora. No geral o sistema me trouxe a empresa na palma da mão.”"
              />
              <TestimonialCard
                image="/images/testimonials/user.png"
                alt="Depoimento Cleiton Andreatta - SOS Mercês"
                link="https://blog.satellitus.com/index.php/2021/07/27/armazenamento-em-nuvem-do-autem-e-fundamental-para-operacao-da-sos-merces/"
                title="Cleiton"
                subtitle="SOS Mercês"
                text="“Com o armazenamento em nuvem nós não precisamos mais nos preocupar em investir em um servidor próprio. Além disso, nós conseguimos acessar os dados do AutEM de qualquer lugar, só precisamos ter acesso a internet.”"
              />
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}