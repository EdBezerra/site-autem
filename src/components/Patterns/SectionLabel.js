import styles from '../../styles/components/Patterns/SectionLabel.module.css';

export default function SectionLabel({text}) {

  return(
    <span className={styles.span}>
      {text}
    </span>
  );
}
