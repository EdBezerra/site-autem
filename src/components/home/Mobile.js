import { useContext, useRef, useState } from 'react';
import { ModalTriggerContext } from '../../contexts/ModalTriggerContext';
import styles from '../../styles/components/home/Mobile.module.css';
import ReactTooltip from 'react-tooltip';

export default function Mobile() {

  const { closeImmersion, isClicked, setIsClicked } = useContext(ModalTriggerContext);
  const plate = useRef();

  const [validate, setValidate] = useState(0)

  function validatePlate() {
    if (plate.current.value.length == 7) {
      setValidate(1)
    }
  }

  function changeStatus() {
    setValidate(validate + 1)
  }

  function status() {
    switch(validate) {
      case 0:
        return <div className={styles.plateValidation}>
          <input onKeyPress={validatePlate} ref={plate} maxLength="7" placeholder="Valide a placa" />
          <button><img src="/icons/next.png" /></button>
        </div>;
      case 1:
        return <div className={styles.afterValidate}>
          <button onClick={changeStatus}>No destino</button>
        </div>
        ;
      case 2:
        return <div className={styles.afterValidate}>
          <button onClick={changeStatus}>Finalizar</button>
        </div>
        ;
      case 3:
        return <div className={styles.afterValidate}>
          <p>Serviço finalizado com sucesso!</p>
        </div>
        ;
    }
  }


  return(
    <div className={styles.overlay}>
      <div className={styles.phone}>
        <a
          className={styles.closeButton}
          type="button"
          onClick={closeImmersion}
        >
          <img src="/icons/close.svg" alt="Fechar Simulação" />
        </a>
        <div className={styles.screen}>
          <div className={styles.app}>
            <div className={styles.appHeader}>
              <div className={styles.user}>
                <div className={styles.userIcon}>
                  <img src="/images/sat.jpg" />
                </div>
                <div className={styles.userInfo}>
                  <p>Olá, <span>Sat</span></p>
                  <span>ABC1234</span>
                </div>
              </div>
              <div>
                <img src="/icons/settings.png" />
              </div>
            </div>

            <div className={styles.appBody}>
              {
                isClicked == false ? (
                  <>
                    <div className={styles.hello}>
                      <span>Serviços</span>
                      <span><strong>Quarta-feira</strong>, 10 de junho.</span>
                    </div>
    
                    <div className={styles.newService}>
                      <div className={styles.serviceInfo}>
                        <span>Duster</span>
                        <span>14:23h</span>
                      </div>
                      <div className={styles.serviceType}>
                        <div>
                          <img src="/icons/wheel.png" />
                          Reboque Leve
                        </div>
                        <div>
                          <img src="/icons/plot-white.svg" />
                          Avenida 9 de Julho
                          <img className={styles.nextArrow} src="/icons/next-white.png" />
                        </div>
                      </div>
                    </div>
                    
                    <div className={styles.buttonAcceptService}>
                      <button onClick={closeImmersion}>Recusar</button>
                      <button onClick={setIsClicked}>Aceitar</button>
                    </div>
      
                    <div className={`${styles.oldService} ${styles.active}`}>
                      <div className={styles.serviceInfoAlt}>
                        <span>HB20</span>
                        <span>17:48h</span>
                      </div>
                      <div className={styles.serviceType}>
                        <div>
                          <img src="/icons/wheel-alt.png" />
                          Reparo Residêncial
                        </div>
                        <div>
                          <img src="/icons/plot-alt.svg" />
                          Rua Jarinu
                          <img className={styles.nextArrow} src="/icons/next.png" />
                        </div>
                      </div>
                    </div>
      
                    <div className={`${styles.oldService} ${styles.canceled}`}>
                      <div className={styles.serviceInfoAlt}>
                        <span>Volvo XC9</span>
                        <span>09:36h</span>
                      </div>
                      <div className={styles.serviceType}>
                        <div>
                          <img src="/icons/wheel-alt.png" />
                          Reparo Mecânico
                        </div>
                        <div>
                          <img src="/icons/plot-alt.svg" />
                            Avenida Jundiaí
                          <img className={styles.nextArrow} src="/icons/next.png" />
                        </div>
                      </div>
                    </div>
                  </>

                ) : (
                  <>
                    <div className={styles.hello}>
                      <span>Duster</span>
                      <span>
                        {validate <= 2 ? "Em andamento" : "Finalizado"}
                      </span>

                      <div className={styles.serviceStatus}>
                        <div className={styles.dotOne}></div>
                        <div className={styles.dotTwo}></div>
                        <div className={styles.dotThree}></div>
                        <div className={styles.dotFour}></div>
                      </div>
                    </div>

                    <>
                      {status()}
                    </>

                    <div className={styles.mobileButtons}>
                      <a
                        className={styles.appButton}
                        data-tip data-for='checklist'
                        href="https://satellitus.com/blog/index.php/2019/10/25/checklist-digital-como-a-tecnologia-pode-ajudar-na-gestao-dos-seus-servicos/"
                        target="__blank"
                      >
                        <img src="/icons/mobile-checklist.svg" />
                      </a>
                      <ReactTooltip id="checklist">
                        Checklist digital.
                      </ReactTooltip>
                      <a
                        className={styles.appButton}
                        data-tip data-for='message'
                        href="https://satellitus.com/blog/index.php/2019/10/30/autem-mobile-como-nosso-aplicativo-funciona/"
                        target="__blank"
                      >
                        <img src="/icons/support.svg" />
                      </a>
                      <ReactTooltip id="message">
                        Mensagens.
                      </ReactTooltip>
                      <a
                        className={styles.appButton}
                        data-tip data-for='maps'
                        href="https://satellitus.com/blog/index.php/2019/11/18/localizacao-da-equipe-em-tempo-real-saiba-qual-a-importancia/"
                        target="__blank"
                      >
                        <img src="/icons/finish.svg" />
                      </a>
                      <ReactTooltip id="maps">
                        Roteirização.
                      </ReactTooltip>
                      <a
                        className={styles.appButton}
                        data-tip data-for='km'
                        href="https://satellitus.com/blog/index.php/2019/10/30/autem-mobile-como-nosso-aplicativo-funciona/"
                        target="__blank"
                      >
                        KM
                      </a>
                      <ReactTooltip id="km">
                        Quilometragem do serviço.
                      </ReactTooltip>
                      <a
                        className={styles.appButton}
                        data-tip data-for='gallery'
                        href="https://satellitus.com/blog/index.php/2019/10/30/autem-mobile-como-nosso-aplicativo-funciona/"
                        target="__blank"
                      >
                        <img src="/icons/picture.svg" />
                      </a>
                      <ReactTooltip id="gallery">
                        Fotos do atendimento
                      </ReactTooltip>
                    </div>

                    <div>
                      <div className={styles.serviceInformation}>
                        <div className={styles.serviceInformationHeader}>
                          <img src="/icons/info.svg" />
                          Atendimento
                        </div>
                        <div><strong>Assistência: </strong>Lorem</div>
                        <div><strong>Produto: </strong>Lorem</div>
                        <div><strong>Número: </strong>78911254</div>
                        <div><strong>Data e Hora: </strong>06/08 às 11:00h</div>
                        <div><strong>Previsão: </strong>40min.</div>
                        <div><strong>Tipo: </strong>Reboque Leve</div>
                        
                        <div className={styles.divisory}></div>

                        <div className={styles.serviceInformationHeader}>
                          <img src="/icons/info.svg" />
                          Cliente
                        </div>
                        <div><strong>Nome: </strong>Carlos</div>
                        <div><strong>Telefone: </strong>(11) 4230-0152)</div>
                        <div><strong>Veículo: </strong>Volvo XC90</div>
                        <div><strong>Cor: </strong>Prata</div>
                        <div><strong>Observação: </strong>Carro tombado</div>
                      </div>
                    </div>
                  </>
                )
              }
            </div>

            <div className={styles.appNavbar}>
              <img src="/icons/car.svg" />
              <img src="/icons/box.svg" />
              <img src="/icons/calendar.svg" />
              <img src="/icons/notification.png" />
            </div>
          </div>
          <div className={styles.notch}></div>
        </div>
        <div className={styles.lineUp}></div>
				<div className={`${styles.lineUp} ${styles.lineDown}`}></div>
        <button className={styles.turnOff} onClick={closeImmersion}/>
      </div>
      <div className={styles.sat}>
        <img src="images/sat-body.png" />
      </div>
      <div className={styles.message}>
        {
          isClicked == false ? (
            <>
              Esse é o AutEM Mobile, é por aqui que seus profissionais
              vão receber todos os serviços!
              <br></br><br></br>
              Clique em <strong>aceitar </strong>
              para prosseguir.
            </>
          ) : (
            <>
              Informações do atendimento, validação de placa, checklist, fotos e muito mais!
              <br></br><br></br>
              Com o AutEM, você não tem com o que se preocupar.
            </>
          )
        }
      </div>
    </div>
  );
}