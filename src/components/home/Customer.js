import styles from '../../styles/components/home/Customer.module.css';
import SectionLabel from '../Patterns/SectionLabel';

export default function Customer() {
  return(
    <div className={styles.customer}>
      <div className={styles.container}>
        <div data-aos="fade-right">
          <SectionLabel
            text="CLIENTES"
          />
        </div>
        <div data-aos="fade-left">
          <img src="/images/customer/logo_ike.png" alt="Ikê Assistência - Cliente AutEM" loading="lazy"/>
          <img src="/images/customer/logo_europ.png" alt="Europ Assistance - Cliente AutEM" loading="lazy"/>
          <img src="/images/customer/logo_facil.png" alt="Fácil Assist - Cliente AutEM" loading="lazy"/>
          <img src="/images/customer/logo_mapfre.png" alt="Mapfre - Cliente AutEM" loading="lazy"/>
          <img src="/images/customer/logo_mondial.png" alt="Mondial Assistance - Cliente AutEM" loading="lazy"/>
          <img src="/images/customer/logo_tempo.png" alt="Tempo Assist - Cliente AutEM" loading="lazy"/>
        </div>
        <img className={styles.element} src="/elements/dotPatternAlt.png"/>
      </div>
    </div>
  );
}