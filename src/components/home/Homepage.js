import styles from '../../styles/components/home/Homepage.module.css';
import Button from '../Patterns/Button';
import ButtonSecondary from '../Patterns/ButtonSecondary';
import { useSpring, animated } from 'react-spring';
import { useContext, useEffect } from 'react';
import { ModalContext } from '../../contexts/ModalTrialContext';
import { ModalTrial } from '../ModalTrial';

const calc = (x, y) => [x - window.innerWidth / 2, y - window.innerHeight / 2]
const trans = (x, y) => `translate3d(${x / 20}px,${y / 20}px,0)`;


export default function Homepage() {

  const [props, set] = useSpring(() => ({ xy: [0, 0], config: { mass: 10, tension: 550, friction: 140 } }));
  const { openModal } = useContext(ModalContext);

  return(
    <div className={styles.home} id="home">
      <section className={styles.container} onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}>
        <div className={styles.hero}>
          <h1 data-aos="fade-down">
          Gestão de equipes <br/>
          Aumente a sua produtividade
          </h1>
          <p data-aos="fade-down">
          Uma experiência única para a sua empresa se destacar no mercado, e se tornar referência em prestação de serviços. 
          </p>
          <div data-aos="fade-up" className={styles.containerButton}>
            <Button
              text="Teste grátis"
              type="button"
              action={openModal}
            />
            <ButtonSecondary
              text="Contate-nos"
              link="contact"
            />
          </div>
        </div>
        <div className={styles.imageContainer}>
          <div data-aos="fade-right" className={styles.social}>
            <a href="https://www.instagram.com/autemgi/">
              <img src="/icons/social/Instagram.svg" alt="Instagram AutEM" loading="lazy"/>
            </a>
            <a href="https://www.facebook.com/satellitustecnologia/">
              <img src="/icons/social/Facebook.svg" alt="Facebook Satellitus" loading="lazy"/>
            </a>
            <a href="https://www.linkedin.com/company/satellitus/">
              <img src="/icons/social/LinkedIn.svg" alt="LinkedIn Satellitus" loading="lazy"/>
            </a>
          </div>
          <img data-aos="zoom-in" className={styles.imageDesktop} src="/images/home.png" alt="Rastreamento em tempo real - AutEM" loading="lazy"/>
          <div data-aos="zoom-in-left" className={styles.imageMobile}>
            <animated.img src="/images/homeMobile.png" alt="AutEM Mobile" loading="lazy" style={{ transform: props.xy.to(trans) }}/>
          </div>
          <div className={styles.imagePopContainer}>
            <animated.div style={{ transform: props.xy.to(trans) }}>
              <img src="/images/homeCard.png" alt="Card tempo médio de chegada - AutEM"  loading="lazy"/>
              <img src="/images/homeCardAlt.png" alt="Card serviços em andamento - AutEM" loading="lazy"/>
            </animated.div>
          </div>
        </div>

        <img className={`${styles.element} + ${styles.elementOne}`} src="/elements/dotPattern.png"/>
        <img className={`${styles.element} + ${styles.elementTwo}`} src="/elements/dotPatternAlt.png"/>
        <img className={`${styles.element} + ${styles.elementThree}`} src="/elements/containerHero.png"/>
        <img className={`${styles.element} + ${styles.elementFour}`} src="/elements/containerHeroTwo.png"/>
        <img className={`${styles.element} + ${styles.elementFive}`} src="/elements/containerHero.png"/>
      </section>
    </div>
  );
}