import styles from '../../styles/components/Patterns/ButtonSecondary.module.css';
import { Link } from 'react-scroll';

export default function ButtonSecondary({text, link, action}) {
  return(
    <div className={styles.button}>
      <Link
        to={link} smooth={true} duration={1000}
      >
        {text}
      </Link>
    </div>
  );
}
