import styles from '../../styles/components/home/Trial.module.css';
import SectionLabel from '../Patterns/SectionLabel';
import Button from '../Patterns/Button';
import ButtonSecondary from '../Patterns/ButtonSecondary';
import { useContext, useEffect, useRef, useState } from 'react';
import { ModalContext } from '../../contexts/ModalTrialContext';
import { ModalTriggerContext } from '../../contexts/ModalTriggerContext';
import { ModalTrial } from '../ModalTrial';
import { ModalTrigger } from '../home/ModalTrigger';
import { useSpring, animated } from 'react-spring';

const calc = (x, y) => [x - window.innerWidth / 2, y - window.innerHeight / 2]
const trans = (x, y) => `translate3d(${x / 20}px,${y / 20}px,0)`;

export default function Trial() {
  const [props, set] = useSpring(() => ({ xy: [0, 0], config: { mass: 10, tension: 550, friction: 140 } }));
  const { openModal } = useContext(ModalContext);
  const {
    openModalTrigger,
    sendService,
    openImmersion,
    closeImmersion,
    buttonAccept,
    isClicked,
    closeNotification,
    sat,
    closeMessage,
  } = useContext(ModalTriggerContext);

  const trialSection = useRef();
  const satContainer = useRef();

  useEffect(() => {
    setTimeout(() => {
      if(sat == true && window.innerWidth > 768) {
        window.addEventListener('scroll', function() {
          if(window.pageYOffset + (window.innerHeight * 0.75) > trialSection.current.offsetTop) {
            satContainer.current.style.display = "block"
          } else {
            satContainer.current.style.display = "none"
          }
        });
      } else {
        return
      }
    }, 1000)
  }, []);

  
  return(
    <div className={styles.trial} ref={trialSection}>
      <div ref={satContainer} className={styles.sat}>
        {
          sat == true ? (
            <>
              <img src="/images/sat-body.png" />
              <div className={styles.message}>
                Olá, eu sou o Sat! Gostaria de simular um atendimento?
                <div className={styles.immersionButtons}>
                  <button onClick={closeMessage}>Não</button>
                  <button onClick={openModalTrigger}>Sim</button>
                </div>
              </div>
            </>
        ) : (
          <></>
          )
        }
      </div>
      <div className={styles.container} onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}>
        <section>
          <div data-aos="fade-right">
            <img className={styles.trialImage} src="/images/painel.png" />
            <animated.div className={styles.serviceContainer} style={{ transform: props.xy.to(trans) }}>
              <div>
                <img src="/images/serviceAlt.png" />
                <>
                  {isClicked == false ? (
                    <div className={styles.triggerButton}>
                      <button onClick={openModalTrigger}>
                        <img src="icons/spinner.png" />
                        NÃO ENVIADO
                        <img src="icons/location-arrow.svg" />
                      </button>
                    </div>
                  ) : (
                    <div className={styles.triggerButtonAccepted}>
                      <div>
                        <img src="icons/check-white.svg" />
                        <span>
                          ACEITO
                        </span>
                      </div>
                      <div>
                        <img src="icons/location-arrow-grey.svg" />
                      </div>
                    </div>
                    )
                  }
                </>
              </div>
            </animated.div>
          </div>
          <div data-aos="fade-left" className={styles.trialInfo}>
            <SectionLabel
              text="TESTE GRÁTIS"
            />
            <h2>
              E aí, vamos fazer um teste?
            </h2>
            <div className={styles.actionButtons}>
              <Button
                text="Comece já"
                action={openModal}
              />
              <ButtonSecondary
                text="Contate-nos"
                link="contact"
              />
            </div>
            <div className={styles.noRequirements}>
              <span>
                <img src="/icons/check.svg" />
                7 dias grátis
              </span>
              <span>
                <img src="/icons/check.svg" />
                Sem cartão
              </span>
            </div>
          </div>
        </section>

        {
          sendService == true ? (
            <div className={styles.notification}>
              <div className={styles.notificationInfo}>
                <div>
                  <img src="icons/autem-icon.svg"/>
                </div>
                <div>
                  <h5>AutEM Mobile</h5>
                  <span>Legal, você tem um novo serviço!</span>
                </div>
              </div>
              <div className={styles.acceptService}>
                <button onClick={openImmersion}>
                  Abrir
                </button>
                <div className={styles.divider}></div>
                <button onClick={closeNotification}>
                  Fechar
                </button>
              </div>
            </div>
          ) : (
            <>
            </>
          )
        }

        <img className={`${styles.element} + ${styles.elementOne}`} src="/elements/dotPatternBlack.png" />
        <img className={`${styles.element} + ${styles.elementTwo}`} src="/elements/containerHero.png" />
      </div>
    </div>
  );
}