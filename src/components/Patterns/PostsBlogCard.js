import styles from '../../styles/components/Patterns/PostsBlogCard.module.css';

export default function PostsBlogCard({ image, alt, title, dt_pub, text, link, }) {

  return (
    // image, alt, title, dt_pub, text, link,
    <div className={styles.card} onClick={() => { link ? window.open(link, '_blank') : null }}>
      <div className={styles.cardInfo}>
        <img src={image} alt={alt} />
        <span>{dt_pub}</span>
        <h3>{title}</h3>
        <div className={styles.cardContent}>
          <div className={styles.cardText}>
            <p>{text}</p>
          </div>
          <span><a href={link}>Leia mais</a></span>
        </div>
      </div>
    </div>
  );
}