import styles from '../../styles/components/Patterns/TestimonialCard.module.css';

export default function TestimonialCard({ image, alt, title, subtitle, text, link}) {

  return(
    <div className={styles.card} onClick={() => {link ? window.location = link : null}}>
      <div className={styles.cardInfo}>
        <img src={image} alt={alt}/>
        <div>
          <h3>{title}</h3>
          <span>{subtitle}</span>
        </div>
      </div>
      <div className={styles.cardText}>
        <p>
          {text}
        </p>
      </div>
    </div>
  );
}