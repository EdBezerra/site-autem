import styles from '../styles/components/ModalTrial.module.css';
import SectionLabel from './Patterns/SectionLabel';

import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import axios from 'axios';
import { useContext, useEffect, useRef, useState } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ModalContext } from '../contexts/ModalTrialContext';
import InputMask from "react-input-mask";

export function ModalTrial() {
  const { closeModal } = useContext(ModalContext)

  const notifySuccess = () => {
    toast.success("🚀 Tudo certo! Em breve nossa equipe entrará em contato", {
      position: toast.POSITION.TOP_RIGHT,
      pauseOnHover: true
    });
  }

  const notifyError = () => {
    toast.error("😥 Ops, não conseguimos completar a sua solicitação...", {
      position: toast.POSITION.TOP_RIGHT,
      pauseOnHover: true
    });
  }

  const notifyInfo = () => {
    toast.info("👨‍🚀 Aguarde, nossos astronautas estão trabalhando!", {
      position: toast.POSITION.TOP_RIGHT,
      pauseOnHover: true
    });
  }

  const alreadyExist = () => {
    toast.info("😥 Já existe um cliente com esse CNPJ!", {
      position: toast.POSITION.TOP_RIGHT,
      pauseOnHover: true
    });
  }

  const schema = yup.object().shape({
    responsible: yup.string().required('Nome é obrigatório.').min(3, 'Digite um nome válido.'),
    email: yup.string().email('Digite um e-mail válido.').required('E-mail é obrigatório.'),
    phone: yup.string().min(10, 'Digite um telefone válido').required('Telefone é obrigatório.'),
    cpf_cnpj: yup.string().required('CNPJ é obrigatório.')
  })

  const {
    register,
    handleSubmit,
    reset,
    formState:{
      errors,
    } 
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: { something: "anything" }
  });

  const [submittedData, setSubmittedData] = useState({});

  const onSubmit = (data) => {
    notifyInfo();
    setSubmittedData(data);
    axios({
      method: 'post',
      url: 'http://localhost:8080/trial_register',
      data: {
        responsible: data.responsible,
        email: data.email,
        phone: data.phone,
        cpf_cnpj: data.cpf_cnpj.replace(/[^\d]+/g,'')
      },
    })
    .then(function (response) {
      if(response.status === 207) {
        alreadyExist();
      } else {
        notifySuccess();
        reset({ ...submittedData });
      }
    })
    .catch(function (error) {
      notifyError();
    });
  };

  function validate(evt) {
    var theEvent = evt || window.event;
  
    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
    // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  return(
    <div className={styles.overlay}>
      <div className={styles.container}>
        <button
          className={styles.closeButton}
          type="button"
          onClick={closeModal}
        >
          <img src="/icons/close.svg" alt="Fechar modal" />
        </button>

        <section>
          <div className={styles.modalTrialInfo}>
            <SectionLabel
              text="TESTE GRÁTIS"
            />
            <img src="/images/trial-image.png" />
            <h2>Comece seu teste já!</h2>
            <p>Conheça o AutEM por 7 dias sem nenhum compromisso.</p>
          </div>

          <div>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className={styles.formInput}>
                <label>Nome</label>
                <input
                  type="text"
                  placeholder="Nome"
                  name="responsible"
                  {...register("responsible")}
                  />
                  <p className={styles.error}>{errors.responsible?.message}</p>
              </div>

              <div className={styles.formInput}>
               <label>CNPJ</label>
                <InputMask
                  mask="99.999.999/9999-99"
                  placeholder="CNPJ"
                  name="cpf_cnpj"
                  {...register('cpf_cnpj')}
                />
                <p className={styles.error}>{errors.cpf_cnpj?.message}</p>
              </div>

              <div className={styles.formInput}>
                <label>Celular</label>
                <input
                  onKeyPress={validate}
                  maxLength={11}
                  type="tel"
                  placeholder="Celular"
                  name="phone"
                  {...register('phone')}
                />
                <p className={styles.error}>{errors.phone?.message}</p>
              </div>

              <div className={styles.formInput}>
                <label>E-mail</label>
                <input
                  type="email"
                  placeholder="E-mail"
                  name="email"
                  {...register('email')}
                />
                <p className={styles.error}>{errors.email?.message}</p>
              </div>

              <div className={styles.buttonContainer}>
                <button
                  className={styles.sendButton}
                  type="submit"
                  aria-label="Teste grátis"
                >
                  Teste grátis
                </button>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>
  );
}