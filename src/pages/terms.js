import Head from 'next/head';
import styles from '../styles/terms.module.css';

export default function Home() {
  return (
    <>
      <Head>
        <title>Termos de Uso AutEM | Gestão Inteligente</title>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
      </Head>

      <div className={styles.container}>
        <div className={styles.header}>
          <a href="/">
            <img src="/images/logo/autem-sat.svg" />
          </a>
        </div>

        <div className={styles.terms}>
          <h1>Termos e Condições de Uso | Política de Privacidade</h1>
          <p>
            O presente Termo, Condições de Uso e Política de Privacidade
            refere-se ao licenciamento do Software AutEM,  Aplicativo Go AutEM
            e Aplicativo AutEM Mobile (doravante denominado simplesmente “Contrato”),
            a qual rege o licenciamento e uso dos referidos, conforme adiante definido
            neste Contrato, pela Satellitus Consultoria e Tecnologia LTDA
            (“Satellitus”), inscrita no CNPJ/ME sob o nº 24.654.821/0001-00, com
            sede na cidade de Jundiaí/SP, na Avenida Dona Manoela Lacerda de
            Vergueiro nº 58, 2º Andar, Anhangabaú - CEP: 13208-057, à toda e
            qualquer pessoa física ou jurídica (sendo a pessoa jurídica doravante
            denominada responsável “LICENCIADO (A)” e as pessoas físicas usuárias
            dos softwares) para uso, em território nacional, dos serviços que
            possibilitam que empresas e pessoas solicitem serviços de reboque
            e assistência 24 horas, diretamente aos provedores de serviço das
            plataformas de propriedade da Satellitus Consultoria e
            Tecnologia LTDA.
          </p>
          <p>
            Compreende um programa de computador e aplicativos, podendo incluir
            os meios físicos associados, bem como quaisquer materiais impressos
            e qualquer documentação online ou eletrônica. Ao utilizar os SOFTWARES
            mencionados, mesmo que parcialmente ou a título de teste, o LICENCIADO
            RESPONSÁVEL, bem como os USUÁRIOS estarão vinculados aos termos presentes,
            concordando com suas disposições, principalmente com relação ao
            CONSENTIMENTO PARA O ACESSO, COLETA, USO, COMPARTILHAMENTO, ARMAZENAMENTO,
            TRATAMENTO E TÉCNICAS DE PROTEÇÃO ÀS INFORMAÇÕES do LICENCIADO E USUÁRIOS
            pela LICENCIANTE, necessárias para a integral execução das funcionalidades
            oferecidas pelos SOFTWARES. Em caso de discordância com os termos aqui
            apresentados, a utilização do SOFTWARE deve ser imediatamente interrompida
            pelo LICENCIADO.
          </p>

          <p>
            <strong>Software AutEM</strong> - trata-se do programa de computador disponível
            ao LICENCIADO somente à partir do licenciamento devidamente firmado entre a
            Satellitus e o LICENCIADO, após aceite de proposta e pagamentos de implantações
            devidas, para fins de gestão de equipes externas, gestão financeira e gerencial do
            LICENCIADO, pessoa Jurídica.
          </p>

          <p>
            <strong>Aplicativo AutEM Mobile</strong> - trata-se de aplicativo móvel disponível aos USUÁRIOS TÉCNICOS
            do LICENCIADO somente à partir do licenciamento do Software AutEM pela Satellitus ao
            LICENCIADO, para fins de envios e recebimentos de serviços das bases aos técnicos
            que executam os serviços externos, tais como gestão de aceites, acompanhamentos e
            finalização.
          </p>

          <p>
            <strong>Aplicativo Go AutEM</strong> - trata-se de aplicativo móvel disponível aos USUÁRIOS CLIENTES
            ou AUTORIZADOS do LICENCIADO, podendo ser consumidores finais ou não, somente à
            partir do licenciamento do Software AutEM pela Satellitus ao LICENCIADO, bem
            como à partir de compra prévia de pacotes de serviços pelo LICENCIADO,para
            possibilitar que empresas e pessoas solicitem serviços de reboque e assistência
            24 horas, tais como chaveiros, encanadores, eletricistas, entre outros, diretamente
            aos provedores de serviço da plataforma AutEM. O serviço é oferecido para os clientes
            da plataforma AutEM, e estes por sua vez, oferecem para B2C e B2B.
          </p>

          <p><strong>1. Da Propriedade Intelectual</strong></p>
          <p>
            O LICENCIADO e USUÁRIOS não adquirem, pelo presente instrumento ou pela utilização
            dos SOFTWARES, nenhum direito de propriedade intelectual ou outros direitos exclusivos,
            incluindo patentes, desenhos, marcas, direitos autorais ou quaisquer direitos sobre
            informações confidenciais ou segredos de negócio, bem como todo o conteúdo disponibilizado
            no Site, incluindo, mas não se limitando a textos, gráficos, imagens, logotipos, ícones,
            fotografias, conteúdo editorial, notificações, softwares e qualquer outro material, sobre
            ou relacionados aos SOFTWARES ou nenhuma parte dele. O LICENCIADO e USUÁRIOS também não
            adquire nenhum direito sobre ou relacionado aos SOFTWARES ou qualquer componente ou módulo
            dele, além dos direitos expressamente licenciados ao LICENCIADO sob o presente termo ou em
            qualquer outro contrato mutuamente acordado por escrito entre o LICENCIADO e a LICENCIANTE.
            Quaisquer direitos não expressamente concedidos sob o presente instrumento são reservados.
          </p>
          <p>
            Também será de propriedade exclusiva da LICENCIANTE, ou está devidamente licenciado,
            todo o conteúdo disponibilizado no site, incluindo, mas não se limitando a textos,
            gráficos, imagens, logos, ícones, fotografias, conteúdo editorial, notificações,
            softwares e qualquer outro material.
          </p>

          <p><strong>2. Declarações do LICENCIADO e USUÁRIOS</strong></p>
          <p>
            O LICENCIADO e USUÁRIOS declaram ter pleno conhecimento dos direitos e obrigações decorrentes
            do presente termo, constituindo este instrumento o acordo completo entre as partes. Declara,
            ainda, ter lido, compreendido e aceito todos os seus termos e condições.
          </p>
          <p>
            O LICENCIADO e USUÁRIOS declaram que foi devidamente informado da política de confidencialidade
            e ambientes de proteção de informações confidenciais, dados pessoais e registros de acesso da
            LICENCIANTE, consentindo livre e expressamente às ações de coleta, uso, compartilhamento,
            armazenamento e tratamento das referidas informações e dados.
          </p>
          <p>
            O LICENCIADO e USUÁRIOS declaram estar cientes de que as operações que correspondam à aceitação
            do presente termo, de determinadas condições e opções, bem como eventual rescisão do presente
            instrumento e demais alterações, serão registradas nos bancos de dados da LICENCIANTE,
            juntamente com a data, hora e endereço de IP em que foram realizadas pelo LICENCIADO, podendo
            tais informações serem utilizadas como prova pelas partes, independentemente do cumprimento de
            qualquer outra formalidade.
          </p>
          <p>
            O LICENCIADO declara que está ciente de que, em qualquer hipótese, deve atender rigorosamente a
            legislação, especialmente no que se refere às suas obrigações tributárias, fiscais, trabalhistas
            e previdenciárias, seja de natureza principal ou acessória, bem como quaisquer outras, entendendo
            que os SOFTWARES, objeto deste termo, trata-se de uma condição de meio e não de resultado, não
            dispensando, portanto, a correta alimentação das informações e parametrizações necessárias pelo
            LICENCIADO com base na legislação em vigor.
          </p>
          <p>
            O LICENCIADO e USUÁRIOS declaram que estão cientes que a LICENCIANTE por definição compartilha
            informações e dados necessários, não sendo esses considerados sensíveis pela Lei Geral de
            Proteção de Dados - LGPD, com EMPRESAS PARCEIRAS para promover novos negócios ao LICENCIADO.
            Embora esses dados e informações possam ser automaticamente compartilhadas por definição, o
            LICENCIADO e USUÁRIOS poderão, a qualquer tempo, solicitar a suspensão do compartilhamento,
            realizando uma solicitação prévia por email: juridico@satellitus.com.
          </p>
          <p>
            O LICENCIADO e USUÁRIOS compreendem e concordam que o uso de suas Informações por EMPRESA PARCEIRA
            coletadas junto ao mesmo (ou conforme autorizado pelo LICENCIADO) é regido pelas políticas de privacidade
            destas EMPRESAS PARCEIRAS e por suas configurações no respectivo serviço, e o uso de tais informações por
            parte do LICENCIADO é regido por esse Termo de Uso e pelas configurações da sua conta no SOFTWARE da LICENCIANTE.
            A LICENCIANTE não será responsável, sob qualquer hipótese, pelo manuseio das informações compartilhadas com
            EMPRESAS PARCEIRAS.
          </p>
          <p>
            O LICENCIADO e USUÁRIOS declaram ainda que estão cientes de que para usufruir de algumas das funcionalidades do
            SOFTWARE, deverá disponibilizar à LICENCIANTE as INFORMAÇÕES para que o SOFTWARE, se mantenha integrado com
            EMPRESAS PARCEIRAS, com as quais mantenha relacionamento, agindo a LICENCIANTE, neste caso, como representante
            e mandatária do LICENCIADO nestes atos.
          </p>
          <p>
            Compartilhamos informações relacionadas aos usuários com terceiros selecionados que nos fornecem uma variedade de
            serviços que suportam a entrega dos nossos serviços (as “EMPRESAS PARCEIRAS”). Essas EMPRESAS PARCEIRAS variam de
            fornecedores de infraestrutura técnica a serviço de atendimento ao cliente e ferramentas de autenticação e muitos
            outros. Asseguramos que o gerenciamento de informações feito em nosso nome por EMPRESAS PARCEIRAS será feito de
            acordo com termos contratuais, que requerem que essas informações sejam mantidas seguras, sejam processadas de
            acordo com as leis de proteção de dados e usadas somente conforme instruímos e não para os propósitos das próprias
            EMPRESAS PARCEIRAS, a menos que tenham autorização do LICENCIADO para tal.
          </p>
          <p>
            EMPRESAS PARCEIRAS podem estar alocadas ou processar suas informações fora do país onde você está baseado.
            Nos casos em que o nosso uso de EMPRESAS PARCEIRAS envolver a transferência de INFORMAÇÕES nós tomaremos
            as medidas necessárias para garantir que esses dados sejam devidamente protegidos.
          </p>
          <p>
            Os tipos de EMPRESAS PARCEIRAS com os quais podemos compartilhar INFORMAÇÕES do LICENCIADO são:
          </p>
          <ol>
            <li>
              Processadores de pagamento acionados por nós para armazenar ou gerenciar informações de
              pagamento com segurança, como detalhes de cartão de crédito ou débito.
            </li>
            <li>
              Instituições financeiras acionadas por nós para armazenar ou gerenciar informações de
              pagamento via boleto bancário.
            </li>
            <li>
              Fornecedores de gerenciamento de e-mail e ferramentas de distribuição, por exemplo, se
              você assinar o recebimento de newsletters da LICENCIANTE ou outras mensagens comerciais,
              gerenciamos seu envio usando uma ferramenta terceirizada de distribuição de e-mail;
            </li>
            <li>
              Fornecedores de serviços de segurança e prevenção de fraudes. Por exemplo, usamos
              esses fornecedores para identificar agentes de software automatizado que podem
              prejudicar nossos serviços ou para prevenir o uso indevido de nossas APIs;
            </li>
            <li>
              Fornecedores de plataformas de software que nos ajudam na comunicação ou no
              fornecimento de serviços de atendimento ao cliente, por exemplo, gerenciamos
              e respondemos todas as mensagens enviadas por meio da nossa central de ajuda
              usando uma ferramenta terceirizada de gerenciamento de comunicações;
            </li>
            <li>
              Fornecedores que possam agregar valor ao LICENCIADO como geração de leads,
              negócios entre outros que a LICENCIANTE vier a negociar.
            </li>
            <li>
              Fornecedores de serviços de armazenamento na nuvem online e outros serviços
              de TI essenciais.
            </li>
          </ol>
          <p>
            Ao LICENCIADO e USUÁRIOS: como processamos suas INFORMAÇÕES somente com a sua
            autorização, você pode retirar essa autorização a qualquer momento usando a
            funcionalidade disponível no recurso respectivo do produto ou entrando em
            contato através do seguinte endereço eletrônico: juridico@satellitus.com.
            A partir do momento que você retira alguma autorização poderá ficar impedido
            de gerar automaticamente alguma atividade que era realizada de forma automatizada.
            Ao revogar seu consentimento, você não vai mais poder usar os serviços e recursos
            que exigem a coleta ou o uso das informações que coletamos ou usamos com base
            no consentimento. O LICENCIADO ainda poderá visualizar a lista de EMPRESAS
            PARCEIRAS e escolher quais gostaria de ativar ou inativar o compartilhamento
            de informações.
          </p>
          <p><strong>3. Licença de Uso do Software</strong></p>
          <p>
            Sujeito aos termos e condições aqui estabelecidos, este termo concede ao
            LICENCIADO uma licença revogável, não exclusiva e intransferível para
            uso dos SOFTWARES. O LICENCIADO não poderá utilizar e nem permitir que
            os SOFTWARES sejam utilizados para outra finalidade que não seja o
            processamento de suas informações ou de pessoas jurídicas indicadas
            pelo LICENCIADO no ato do cadastramento. Esta licença não implica a
            capacidade de acessar outros softwares além daqueles originalmente
            localizados no SOFTWARE. Em nenhuma hipótese o LICENCIADO terá acesso
            ao código fonte do SOFTWARE ora licenciado, por este se tratar de
            propriedade intelectual da LICENCIANTE.
          </p>
          <p>
            A Satellitus não presta e não assegura a prestação de qualquer serviço
            de reboque e assistência 24 horas, sendo responsável apenas pelos
            SOFTWARES licenciados. Os aplicativos não devem ser utilizados pelo
            LICENCIADO e USUÁRIOS para qualquer finalidade que não as expressamente
            autorizadas por este termo. A Satellitus não se responsabiliza por
            quaisquer perdas, prejuízos ou danos decorrentes ou relativos aos
            serviços de reboque e assistência 24 horas.
          </p>

          <p>
            O LICENCIADO e USUÁRIOS deverão fornecer à Satellitus informações de
            cadastro, conforme requerido pela Satellitus. A adesão aos aplicativos
            e usos estarão sujeitos à análise cadastral e seus parceiros de negócios,
            conforme critério exclusivo da Satellitus. Ao solicitar a sua adesão,
            o LICENCIADO e USUÁRIOS expressamente autorizam e concordam com tal
            avaliação nos termos desta disposição e se responsabilizam pelas
            informações fornecidas, declarando serem verdadeiras, sob as penas
            da lei.
          </p>

          <p><strong>4. Das Restrições</strong></p>
          <p>
            Em hipótese alguma é permitido ao LICENCIADO e USUÁRIOS ou a terceiros, de forma geral:   
          </p>
          <ol>
            <li>
              Copiar, ceder, sublicenciar, vender, dar em locação ou em
              garantia, reproduzir, doar, alienar de qualquer forma,
              transferir total ou parcialmente, sob quaisquer modalidades,
              gratuita ou onerosamente, provisória ou permanentemente,
              os SOFTWARES objeto deste EULA, assim como seus módulos,
              partes, manuais ou quaisquer informações a ele relativas;
            </li>
            <li>
              Retirar ou alterar, total ou parcialmente, os avisos
              de reserva de direito existente nos SOFTWARES e na
              documentação;
            </li>
            <li>
              Praticar engenharia reversa, descompilação ou desmontagem
              dos SOFTWARES.
            </li>
          </ol>

          <p><strong>5. Do Prazo</strong></p>
          <p>
            O presente termo entra em vigor na data de seu aceite
            pelo LICENCIADO e USUÁRIOS e vigorará por prazo indeterminado.
            Havendo atualizações das disposições do termo, o LICENCIADO e
            USUÁRIOS serão notificados previamente. Caso o LICENCIADO e
            USUÁRIOS não se manifestem expressamente em contrário, nos
            mesmos termos e condições, inclusive no que se refere à
            eventuais penalidades, estará consentindo na aplicação das
            atualizações.
          </p>

          <p><strong>6. Da Remuneração e Forma de Pagamento</strong></p>
          <p>
            O LICENCIADO pagará à LICENCIANTE, os valores contidos no e-mail
            de contratação e propostas. Neste e-mail constam os detalhes da
            contratação bem como as informações de acesso aos SOFTWARES.
            Ao acessar os SOFTWARES pela primeira vez, o LICENCIADO e
            USUÁRIOS irão visualizar o presente termo.
          </p>

          <p>
            A falta de pagamento de quaisquer valores nas respectivas
            datas de vencimento acarreta ao LICENCIADO o acréscimo de
            multa de 10% (dez por cento) sobre o valor em atraso e
            juros de 1% (um por cento) ao mês calculado "pro rata
            temporis". Caso a pendência financeira seja encaminhada
            a um departamento jurídico poderá acarretar a cobrança
            de 20% (vinte por cento) sobre o cálculo do valor devido,
            referente ao pagamento dos honorários advocatícios, e mais
            o custo com todas as despesas judiciais e/ou extrajudiciais.
          </p>

          <p>
            A falta de pagamento de quaisquer valores nas respectivas
            datas de vencimento não acarretará na rescisão automática
            do presente termo, mas poderá resultar na suspensão imediata
            do acesso do LICENCIADO e USUÁRIOS aos SOFTWARES até que
            as pendências financeiras tenham sido regularizadas. O
            acesso ao SOFTWARE somente será restabelecido após a
            identificação pela LICENCIANTE do pagamento integral de
            todos os valores devidos enquanto o mesmo estiver
            suspenso. A identificação poderá ocorrer em até 2
            (dois) dias úteis após a data de pagamento pelo LICENCIADO.
            Caso o LICENCIADO não resolva a pendência financeira no prazo
            de 90 (noventa) dias contados do vencimento do valor não pago,
            a LICENCIANTE se reservam o direito de rescindir o presente
            termo firmado e apagar de forma definitiva e irrecuperável
            todas as informações do LICENCIADO que por ventura estejam
            armazenadas nos SOFTWARES.
          </p>

          <p>
            O plano contratado pelo LICENCIADO está diretamente vinculado
            à quantidade de serviços que este executarão, viaturas que
            utilizarão, profissionais que atuarão e usuários que
            acessarão os SOFTWARES. Havendo alteração em uma destas
            quantidades, o plano do LICENCIADO deverá ser migrado
            com valor do plano à época da contratação, seja para um
            plano superior ou inferior.
          </p>

          <p>
            Caso o LICENCIADO, no decorrer da vigência do presente instrumento,
            opte por outro plano de superior (upgrade) ou inferior (downgrade),
            os valores serão alterados de acordo com o respectivo plano
            escolhido e os valores serão calculados "pro rata temporis"
            sobre o plano anterior e o plano atualizado.
          </p>

          <p>
            O valor do plano ora contratado pelo LICENCIADO poderá ser
            atualizado anualmente pelos índices de reajuste acumulados
            no período, acrescido do percentual de variação do dólar,
            mediante um aviso prévio de 3 (três) dias antes da data
            de vencimento.
          </p>

          <p>
            Os valores dos planos superiores ou inferiores ao contratado
            também poderão ser atualizados a qualquer momento pela
            LICENCIANTE, sem aviso prévio ao LICENCIADO. 
          </p>

          <p>
            A LICENCIANTE poderá, a seu critério, por mera liberalidade,
            oferecer descontos nos valores dos planos. A LICENCIANTE
            poderá renovar ou não os percentuais de desconto, podendo
            ainda cancelar os descontos após o período de validade,
            sem aviso prévio.
          </p>

          <p>
            O LICENCIADO receberá por e-mail a nota fiscal de serviço
            (a "NFs") bem como o meio de pagamento em no máximo 3 (três)
            dias antes da data de vencimento. A NFs e o boleto de pagamento
            ou link para pagamento também estarão disponíveis para o
            LICENCIADO acessar no SOFTWARE.
          </p>

          <p>
            Mediante atraso nos pagamentos, a LICENCIANTE poderá enviar
            aos usuários da LICENCIADAS via e-mail, sms e/ou avisos no
            SOFTWARE comunicados reportando a situação de inadimplemento.
          </p>

          <p><strong>7. Restituição das Informações</strong></p>
          <p>
          Suspenso o acesso do LICENCIADO ao SOFTWARE, a LICENCIANTE manterá as informações do LICENCIADO armazenadas no SOFTWARE pelo período de 30 (trinta) dias, contados da suspensão de acesso. 
          </p>
          <p>
          Conforme descrito na cláusula 6 acima, passados 30 (trinta) dias da suspensão do acesso do LICENCIADO aos SOFTWARES, todas as INFORMAÇÕES do LICENCIADO em poder da LICENCIANTE serão excluídas permanentemente do banco de dados, independentemente de terem sido extraídas ou não pelo LICENCIADO.
          </p>
          <p>
          Não obstante o disposto acima, as informações referentes à data e hora de acesso e ao endereço de protocolo de internet utilizado pelo LICENCIADO para acessar o Site e os SOFTWARES permanecerão armazenadas pela LICENCIANTE por 6 (meses) a contar da data de cada acesso realizado, independentemente do término da relação jurídica e comercial entre a LICENCIANTE e o LICENCIADO, em cumprimento ao disposto no Artigo 15 da Lei nº 12.965/2014, podendo ser armazenados por um período maior de tempo mediante ordem judicial.
          </p>

          <p><strong>8. Das Obrigações do Licenciado</strong></p>
          <p>
          Obriga-se o LICENCIADO a:
          </p>
          <ol>
            <li>
            Manter pessoal treinado para a operação dos SOFTWARES e para a comunicação com a LICENCIANTE e prover, sempre que ocorrerem quaisquer problemas com os SOFTWARES, toda a documentação, relatórios e demais informações que relatem as circunstâncias em que os problemas ocorreram, objetivando facilitar e agilizar os trabalhos;
            </li>
            <li>
            Manter, às suas expensas, linha de telecomunicação, modem, software de comunicação, endereço de correio eletrônico e outros recursos necessários à comunicação com a LICENCIANTE;
            </li>
            <li>
            Responder pelas INFORMAÇÕES inseridas nos SOFTWARES, pelo cadastramento, permissões, senhas e modo de utilização de seus usuários, incluindo, mas não se limitando, às informações fornecidas no que diz respeito aos métodos de pagamento (boletos), números de contas bancárias e a informações financeiras disponibilizadas às autoridades fiscais por meio dos SOFTWARES no que diz respeito à emissão de notas fiscais ou quanto ao cumprimento das obrigações acessórias. A LICENCIANTE em hipótese alguma serão responsáveis pelo conteúdo (INFORMAÇÕES, senhas, cópias de informações, etc.) incluído nos SOFTWARES, não sendo, portanto, estas INFORMAÇÕES revisadas em momento algum. A responsabilidade pelas INFORMAÇÕES inseridas ou excluídas no SOFTWARE é sempre do LICENCIADO, sendo este o único responsável pela realização de backup das informações, especialmente antes da exclusão. A LICENCIANTE não serão responsáveis pelo armazenamento de informações excluídas pelo LICENCIADO;
            </li>
            <li>
            Certificar-se de que não está proibido por determinação legal e/ou contratual de passar INFORMAÇÕES, bem como quaisquer outros dados à LICENCIANTE, necessários para a execução do serviço oferecido por este termo;
            </li>
            <li>
            A qualquer tempo a LICENCIANTE poderão bloquear acesso ao SOFTWARE caso constate qualquer prática pelo LICENCIADO ou terceiros de violação ao presente Termo de uso e/ou qualquer tentativa de fraude ou dê a entender tratar-se de uma tentativa, não reduzindo essa ação a responsabilidade do LICENCIADO sobre seus atos.
            </li>
            <li>
            Não utilizar os SOFTWARES de qualquer forma que possa implicar em ato ilícito, infração, violação de direitos ou danos à LICENCIANTE ou terceiros, incluindo, mas não se limitando ao uso para invasão de dispositivo informático com o objetivo de obter, adulterar ou destruir dados ou informações sem a autorização expressa do titular de tais dados ou do dispositivo ou servidor nos quais estes estejam armazenados;
            </li>
            <li>
            Não publicar, enviar ou transmitir qualquer arquivo que contenha vírus, worms, cavalos de tróia ou qualquer outro programa que possa contaminar, destruir ou interferir no bom funcionamento dos SOFTWARES;
            </li>
            <li>
            Informar a LICENCIANTE sempre que houver qualquer alteração das em sua base contratual fornecidas à LICENCIANTE e que possam impedir, limitar ou prejudicar o acesso da LICENCIANTE às demais informações necessárias para a execução das funcionalidades ofertadas pelos SOFTWARES;
            </li>
            <li>
            Atender rigorosamente a legislação brasileira e toda obrigação legal imposta e/ou decorrente de sua atividade e em razão da utilização destes SOFTWARES.
            </li>
            <li>
            Caso o LICENCIADO acredite que seu login e senha de acesso aos SOFTWARES tenham sido roubados ou sejam de conhecimento de outras pessoas, por qualquer razão, o LICENCIADO deverá imediatamente comunicar tal fato à LICENCIANTE, sem prejuízo da alteração da sua senha imediatamente, por meio dos SOFTWARES.
            </li>
            <li>
            Para a utilização dos recursos oferecidos pelos SOFTWARES, no que diz respeito a emissão, inutilização, correção, contingência, cancelamento de documentos fiscais, detalhamento das regiões atendidas, versões dos SOFTWARES, uso offline, backup das informações implementadas, entre outros, o LICENCIADO deve verificar e se manter informado, através de nossa central de atendimento. Uma vez aceito os termos deste termo, isso garante também a aceitação das especificações do produto conforme definido em nosso site.
            </li>
            <li>
            O LICENCIADO obrigatoriamente deve possuir autorização expressa dos terceiros ao incluir suas informações nos SOFTWARES (informações de viaturas, serviços, profissionais, funcionários, entre outras). A LICENCIANTE poderá a qualquer momento solicitar estas autorizações para resguardo e aplicação da legislação de proteção de dados.
            </li>
          </ol>

          <p><strong>9. Das Obrigações da LICENCIANTE</strong></p>
          <p>
          Obriga-se a LICENCIANTE a:
          </p>
          <ol>
            <li>
            A LICENCIANTE oferece ao LICENCIADO o plano de treinamento nos primeiros 30 (trinta) dias após o primeiro acesso aos SOFTWARES, respeitando as seguintes condições:
            <ul>
              <li>
              O treinamento ocorrerá com um único usuário chave que o LICENCIADO indicará. 
              </li>
              <li>
              Todo o treinamento deverá ocorrer nos primeiros 30 (trinta) dias após o primeiro acesso aos SOFTWARES. Findando os 30 (trinta) dias a LICENCIANTE se reserva no direito de cobrar pelo serviço de treinamento.
              </li>
              <li>
              O treinamento ocorrerá remotamente mediante compartilhamento de telas entre o LICENCIADO e os analistas previamente autorizados pela LICENCIANTE. 
              </li>
            </ul>
            </li>
            <li>
            A responsabilidade dos serviços prestados restringir-se-á ao sistema AutEM exclusivamente, não respondendo por problemas relacionados ao ambiente, como redes, sistemas operacionais, hardware, navegadores de internet, entre outros. Havendo a impossibilidade de treinamento por parte destes fatores, nossa empresa não se compromete em realizá-lo.
            </li>
            <li>
            A LICENCIANTE garante ao LICENCIADO que os SOFTWARES deverão funcionar regularmente, se respeitadas as condições de uso definidas na documentação. Na ocorrência de falhas de programação (“bugs”), a LICENCIANTE obrigar-se-á a corrigir tais falhas, podendo à seu critério substituir a cópia dos programas com falhas por cópias corrigidas;
            </li>
            <li>
            A LICENCIANTE manterá atualizada às funções existentes nos SOFTWARES com relação às variáveis normalmente alteradas pela legislação federal. A interpretação legal das normas editadas pelo governo será efetuada com base no entendimento majoritário dos “usuários” dos SOFTWARES, doutrinadores e jurisprudência pátria. Ficam excluídas da manutenção as alterações nos SOFTWARES originárias de obrigações assumidas pelo LICENCIADO junto a sindicatos ou associações, tais como convenções coletivas de trabalho e outras, bem como necessidades específicas de certos segmentos, além do atendimento de legislações municipais e/ou estaduais.
            </li>
            <li>
            Fornecer, ato contínuo ao aceite deste termo, acesso ao SOFTWARE durante a vigência deste termo;
            </li>
            <li>
            Suspender o acesso aos SOFTWARES do LICENCIADO que esteja desrespeitando as regras de conteúdo aqui estabelecidas ou as normas legais em vigor;
            </li>
            <li>
            Alterar as especificações e/ou características dos SOFTWARES licenciados para a melhoria e/ou correções de erros, de acordo com o plano de produtos definido pela LICENCIANTE;
            </li>
            <li>
              Disponibilizar acesso aos serviços de suporte compreendido de segunda a sexta-feira:
              <ul>
                <li>
                das 09:00h às 18:00h (horário de Brasília), sem intervalo, via chat (localizado no SOFTWARE);
                </li>
                <li>
                das 09:00h às 12:00h e das 13:00h às 18:00h (horário de Brasília) via atendimento telefônico e por meio de correio eletrônico (suporte@satellitus.com), 
                </li>
              </ul>
            </li>
            <li>
            Manter as INFORMAÇÕES COMPARTILHADAS, INFORMAÇÕES DE CONTA e INFORMAÇÕES PESSOAIS do LICENCIADO, bem como seus registros de acesso, em sigilo, sendo que as referidas INFORMAÇÕES serão armazenadas em ambiente seguro, sendo respeitadas a intimidade, a vida privada, a honra e a imagem do LICENCIADO, em conformidade com as disposições da Lei nº 12.965/2014. A LICENCIANTE poderá compartilhar informações da LICENCIADA com fornecedores como consta na cláusula 2, podendo a LICENCIADA revogar estas permissões diretamente pelo SOFTWARE.
            </li>
            <li>
            A LICENCIANTE não será responsável pelo armazenamento de informações excluídas pelo LICENCIADO.
            </li>
          </ol>

          <p><strong>10. Nível de Serviço</strong></p>
          <p>
          A LICENCIANTE empreenderá esforços comercialmente razoáveis para tornar os SOFTWARES disponíveis, no mínimo, 99% (noventa e nove por cento) durante cada Ano de Serviço (conforme definido abaixo) (“Compromisso de Nível de Serviço”). Na hipótese de a LICENCIANTE não cumprir o Compromisso de Nível de Serviço, o LICENCIADO terá o direito de receber o crédito correspondente ao período de indisponibilidade.
          </p>
          <p>
          Por “Ano de Serviço” entende-se os 365 dias precedentes à data de uma reivindicação relacionada ao nível de serviço. Se o LICENCIADO estiver se utilizando dos SOFTWARES durante período inferior a 365 dias, o Ano de Serviço que lhe corresponde será, ainda assim, considerado como os 365 dias precedentes; no entanto, os dias anteriores a seu uso dos serviços serão considerados como de 100% de disponibilidade. Os períodos de inatividade operacional que ocorrerem antes de uma reivindicação bem-sucedida de Crédito de Serviço não poderão ser usados para efeito de reivindicações futuras.
          </p>
          <p>
          O Compromisso de Nível de Serviço não se aplica caso as circunstâncias de indisponibilidade resultem:
          </p>
          <ol>
            <li>
            de uma interrupção do fornecimento de energia elétrica ou paradas emergenciais não superiores a 2 (duas) horas ou que ocorram no período das 24h às 6:00h (horário de Brasília); 
            </li>
            <li>
            forem causadas por fatores que fujam ao razoável controle da LICENCIANTE, inclusive casos de força maior ou de acesso à Internet e problemas correlatos; 
            </li>
            <li>
            resultem de quaisquer atos ou omissões do LICENCIADO, de terceiros ou de aplicativos terceiros;</li>
            <li>
            resultem do equipamento, software ou outras tecnologias que o LICENCIADO usar que impeçam o acesso regular do SOFTWARE;
            </li>
            <li>
            resultem de falhas de instâncias individuais não atribuíveis à indisponibilidade do LICENCIADO;
            </li>
            <li>
            resultem de práticas de gerenciamento da rede que possam afetar sua qualidade.
            </li>
          </ol>

          <p><strong>11. Isenção de Responsabilidade da LICENCIANTE</strong></p>
          <p>A LICENCIANTE não se responsabiliza:</p>
          <ol>
            <li>
            Por falha de operação, operação por pessoas não autorizadas ou qualquer outra causa em que não exista culpa da LICENCIANTE;
            </li>
            <li>
            Pelo cumprimento dos prazos legais do LICENCIADO para a entrega de documentos fiscais ou pagamentos de impostos, bem como datas de faturamento de notas fiscais e afins;
            </li>
            <li>
            Pelos danos ou prejuízos decorrentes de decisões administrativas, gerenciais ou comerciais tomadas com base nas informações fornecidas pelos SOFTWARES;
            </li>
            <li>
            que se conectam com os SOFTWARES por meio de API ou ainda, de fornecedores de telecomunicações do LICENCIADO;
            </li>
            <li>
            Por revisar as INFORMAÇÕES DE CONTA fornecidas pelo LICENCIADO, bem como as INFORMAÇÕES COMPARTILHADAS obtidas junto aos sites e/ou meios virtuais das instituições parceiras, seja no que tange à precisão dos dados, seja quanto à legalidade ou ameaça de violação em função do fornecimento destas informações;
            </li>
            <li>
            Por quaisquer produtos e/ou serviços oferecidos por meio dos sites e/ou meios virtuais das instituições financeiras, ainda que de maneira solidária ou subsidiária;
            </li>
            <li>
            Por eventuais infrações legais cometidas pelo LICENCIADO, de ordem fiscal, tributária, trabalhista, previdenciária, criminal, ou qualquer outra.
            </li>
            <li>
            Pela geração e envio de obrigações fiscais acessórias, cabendo este procedimento somente ao LICENCIADO;
            </li>
            <li>
            O acesso às INFORMAÇÕES DE CONTA e às INFORMAÇÕES COMPARTILHADAS do LICENCIADO dependem de serviços prestados pelas EMPRESAS PARCEIRAS. Sendo assim, a LICENCIANTE não assume qualquer responsabilidade quanto à qualidade, precisão, pontualidade, entrega ou falha na obtenção das referidas INFORMAÇÕES junto aos sites e/ou meios virtuais das instituições financeiras.
            </li>
            <li>
            Conforme o uso do SOFTWARE pelo LICENCIADO, este pode sugerir naturezas de operações ou os impostos de acordo com o histórico de uso. A LICENCIANTE não se responsabiliza pelo aceite e pelo preenchimento incorreto dessas informações, cabendo somente ao LICENCIADO a checagem correta de suas próprias informações inseridas.
            </li>
            <li>
            A LICENCIANTE adota as medidas de segurança adequadas de acordo com os padrões de mercado para a proteção das INFORMAÇÕES do LICENCIADO armazenadas no SOFTWARE, assim como para o acesso às INFORMAÇÕES COMPARTILHADAS do LICENCIADO. Contudo, o LICENCIADO reconhece que nenhum sistema, servidor ou software está absolutamente imune a ataques e/ou invasões de hackers e outros agentes maliciosos, não sendo a LICENCIANTE responsável por qualquer exclusão, obtenção, utilização ou divulgação não autorizada de INFORMAÇÕES resultantes de ataques que a LICENCIANTE não poderia razoavelmente evitar por meio dos referidos padrões de segurança.
            </li>
          </ol>
          
          <p><strong>12. Da Retomada dos Softwares</strong></p>
          <p>
          A LICENCIANTE se reserva o direito de cancelar imediatamente o acesso do LICENCIADO aos SOFTWARES nos casos em que o LICENCIADO usar o SOFTWARE de forma diversa daquela estipulada no presente instrumento.
          </p>

          <p><strong>13. Das Garantias Limitadas</strong></p>
          <p>
          Na extensão máxima permitida pela lei em vigor, os SOFTWARES são fornecidos “no estado em que se encontra” e “conforme a disponibilidade”, com todas as falhas e sem garantia de qualquer espécie.
          </p>
          <p>
          Não obstante o disposto no item 9.a acima, a LICENCIANTE não garante que as funções contidas nos SOFTWARES atendam às suas necessidades, que a operação dos SOFTWARES serão ininterrupta ou livre de erros, que qualquer serviço continuará disponível, que os defeitos no SOFTWARE serão corrigidos ou que os SOFTWARES serão compatíveis ou funcionem com qualquer SOFTWARE, aplicações ou serviços de terceiros.
          </p>
          <p>
          Além disso, o LICENCIADO reconhece que os SOFTWARES não devem ser utilizados ou não são adequados para ser utilizado em situações ou ambientes nos quais a falha ou atrasos, os erros ou inexatidões de conteúdo, dados ou informações fornecidas pelos SOFTWARES possam levar à morte, danos pessoais, ou danos físicos ou ambientais graves, incluindo, mas não se limitando, à operação de instalações nucleares, sistemas de navegação ou de comunicação aérea, controle de tráfego aéreo, sistemas de suporte vital ou de armas.
          </p>

          <p><strong>14. Limitação de Responsabilidade</strong></p>
          <p>
          Em nenhum caso a LICENCIANTE serão responsáveis por danos pessoais ou qualquer prejuízo incidental, especial, indireto ou consequente, incluindo, sem limitação, prejuízos por perda de lucro, corrupção ou perda de dados, falha de transmissão ou recepção de dados, não continuidade do negócio ou qualquer outro prejuízo ou perda comercial, decorrentes ou relacionados ao seu uso ou sua inabilidade em usar os SOFTWARES, por qualquer outro motivo. Sob nenhuma circunstância a responsabilidade integral da LICENCIANTE com relação ao LICENCIADO por todos os danos excederá a quantia correspondente ao último plano de licenciamento pago pelo LICENCIADO à LICENCIANTE pela obtenção da presente licença de SOFTWARES.
          </p>

          <p><strong>15. Consentimento livre, expresso e informado para acesso a informações confidenciais e dados pessoais</strong></p>
          <p>
            <strong>
              <i>
                <u>
                O LICENCIADO e USUÁRIOS, ao aceitar utilizar os SOFTWAREs, além de aceitar integralmente o presente termo, também consente, livre e expressamente, que a LICENCIANTE colete, use, armazene e faça o tratamento de suas INFORMAÇÕES, incluindo seus dados pessoais, financeiros, bancários, de conta, os quais serão necessários para que o serviço ofertado seja prestado em sua integralidade.
                </u>
              </i>
            </strong>
          </p>

          <p>
            <strong>
              <i>
                <u>
                Para tanto, o LICENCIADO e USUÁRIOS consentem, livre e expressamente, em fornecer os dados que permitam o acesso às INFORMAÇÕES necessárias para que os SOFTWARES executem todas as funções para as quais foi projetado.
                </u>
              </i>
            </strong>
          </p>

          <p>
            <strong>
              <i>
                <u>
                Ainda, o LICENCIADO e USUÁRIOS declaram e reconhecem que para a prestação dos serviços e funcionalidades de integração ofertadas pelo SOFTWARE, a LICENCIANTE acessa INFORMAÇÕES COMPARTILHADAS diretamente no site e/ou outros meios virtuais das EMPRESAS PARCEIRAS, sem fazer qualquer emulação de medidas de segurança, utilizando-se apenas das INFORMAÇÕES PESSOAIS, INFORMAÇÕES DE CONTA, bem como outras eventualmente necessárias, fornecidas pelo LICENCIADO, conforme autorizado.                </u>
              </i>
            </strong>
          </p>

          <p>
            <strong>
              <i>
                <u>
                Ainda, o LICENCIADO e USUÁRIOS declaram e reconhecem que para a prestação dos serviços e funcionalidades de integração ofertadas pelo SOFTWARE, a LICENCIANTE acessa INFORMAÇÕES COMPARTILHADAS diretamente no site e/ou outros meios virtuais das EMPRESAS PARCEIRAS, sem fazer qualquer emulação de medidas de segurança, utilizando-se apenas das INFORMAÇÕES PESSOAIS, INFORMAÇÕES DE CONTA, bem como outras eventualmente necessárias, fornecidas pelo LICENCIADO, conforme autorizado.                </u>
              </i>
            </strong>
          </p>

          <p>
            <strong>
              <i>
                <u>
                O LICENCIADO e USUÁRIOS, por meio deste termo e fornecendo as INFORMAÇÕES DE CONTA, autoriza e consente expressamente que a LICENCIANTE acesse suas INFORMAÇÕES DE CONTA e INFORMAÇÕES COMPARTILHADAS na qualidade de mandatária.
                </u>
              </i>
            </strong>
          </p>

          <p>
            <strong>
              <i>
                <u>
                O LICENCIADO e USUÁRIOS consentem que, ao acessar o site da LICENCIANTE, esta poderá coletar informações técnicas de navegação, tais como tipo de navegador do computador utilizado para acesso ao site, endereço de protocolo de Internet, páginas visitadas e tempo médio gasto no site. Tais informações poderão ser usadas para orientar o próprio LICENCIADO e melhorar os serviços ofertados.                </u>
              </i>
            </strong>
          </p>

          <p>
          O LICENCIADO e USUÁRIOS consentem livre e expressamente que suas INFORMAÇÕES poderão ser transferidas a terceiros em decorrência da venda, aquisição, fusão, reorganização societária ou qualquer outra mudança no controle da LICENCIANTE. A LICENCIANTE, contudo, compromete-se, nestes casos, a informar o LICENCIADO.
          </p>

          <p>
          O LICENCIADO consente livre e expressamente que a LICENCIANTE utilize cookies apenas para controlar a audiência e a navegação em seu site e possibilitar a identificação de serviços segmentados e personalizados ao perfil do LICENCIADO. A LICENCIANTE garante que estas informações coletadas por meio de cookies são estatísticas e não pessoais, bem como que não serão utilizadas para propósitos diversos dos expressamente previstos neste termo, comprometendo-se a adotar todas as medidas necessárias a fim de evitar o acesso e o uso de tais informações por quaisquer terceiros, sem a devida autorização.
          </p>

          <p><strong>15.1 Válido somente para LICENCIADOS conectados a uma EMPRESAS PARCEIRAS</strong></p>
          <p>
          Com o intuito de facilitar a comunicação e o tráfego de informações entre o LICENCIADO e as EMPRESAS PARCEIRAS, diminuir entraves burocráticos e margens de erro na entrega das informações importantes, a plataforma da LICENCIANTE permite a liberação pelo LICENCIADO de acesso direto à algumas empresas, permitindo que INFORMAÇÕES COMPARTILHADAS sejam tramitadas entre as EMPRESAS PARCEIRAS e a LICENCIANTE. Por padrão este compartilhamento está ativo e o LICENCIADO autoriza esta conexão, porém o SOFTWARE permite que o LICENCIADO administre esta conexão bem como quiser, permitindo ou não o compartilhamento para cada uma destas EMPRESAS PARCEIRAS.
          </p>

          <p><strong>16. Da Rescisão</strong></p>
          <p>
          Tanto o LICENCIADO como a LICENCIANTE poderão solicitar a rescisão deste EULA, desde que comunique a parte contrária, por escrito, com antecedência mínima de 30 (trinta) dias. Após esta comunicação o LICENCIADO não terá mais acesso ao SOFTWARE.
          </p>
          <p>
          Nenhum valor será devido pela LICENCIANTE em caso de rescisão por parte do LICENCIADO. Havendo débito do LICENCIADO com a LICENCIANTE estes deverão ser quitados no prazo máximo de 30 (trinta) dias após o comunicado de rescisão.
          </p>
          <p>
          A LICENCIANTE poderá rescindir o presente termo a qualquer momento em caso de violação pelo LICENCIADO dos termos e condições ora acordados, ou em caso de atraso de pagamento não sanado no prazo de 30 (trinta) dias, conforme cláusula 6 acima.
          </p>
          <p>
          No caso de rescisão do presente documento, os dados da empresa, financeiros, de serviços, arquivos, checklists e demais informações da LICENCIADO ficarão armazenados em nosso banco de dados pelo período de 30 (trinta) dias. Após este período todas as informações da LICENCIADO serão excluídas permanentemente. 
          </p>
          <p>
          Caso a LICENCIADO queira obter quaisquer informações após a rescisão e durante o período em que os dados se encontram em nosso banco de dados, o mesmo deverá reativar sua conta mediante a contratação de serviços de consulta de dados. Este valor será informado no momento o qual se fizer necessário este procedimento.
          </p>
          <p>
          Recomendamos que antes de efetuar a rescisão deste documento, a LICENCIADA providencie os devidos backups de todas informações em cada módulo do sistema. É possível exportar todos os dados nos relatórios e também em todas as tabelas de cada módulo. Quanto ao check list e arquivos é necessário que estes sejam baixados individualmente de cada atendimento.
          </p>

          <p><strong>17. Das disposições Legais</strong></p>
          <p>
          Caso o LICENCIADO venha a desenvolver um novo módulo ou produto que caracterize cópia, de todo ou em parte, quer seja do dicionário de dados quer seja do programa, será considerado como parte dos SOFTWARES fornecido pela LICENCIANTE, ficando, portanto, sua propriedade incorporada pela LICENCIANTE e seu uso condicionado a estas cláusulas contratuais;
          </p>
          <p>
          Este termo obriga as partes e seus sucessores e somente o LICENCIADO possui licença não exclusiva para a utilização dos SOFTWARES, sendo-lhe, entretanto, vedado transferir os direitos e obrigações acordados por este instrumento. Tal limitação, no entanto, não atinge a LICENCIANTE, que poderá, a qualquer tempo, ceder, no todo ou em parte, os direitos e obrigações inerentes ao presente termo;
          </p>
          <p>
          A tolerância de uma parte para com a outra quanto ao descumprimento de qualquer uma das obrigações assumidas neste instrumento não implicará em novação ou renúncia de direito. A parte tolerante poderá, a qualquer tempo, exigir da outra parte o fiel e cabal cumprimento deste instrumento;
          </p>
          <p>
          Não constitui causa de rescisão contratual o não cumprimento das obrigações aqui assumidas em decorrência de fatos que independem da vontade das partes, tais como os que configuram o caso fortuito ou força maior, conforme previsto no artigo 393 do Código Civil Brasileiro;
          </p>
          <p>
          Se qualquer disposição deste termo for considerada nula, anulável, inválida ou inoperante, nenhuma outra disposição deste termo será afetada como consequência disso e, portanto, as disposições restantes deste termo permanecerão em pleno vigor e efeito como se tal disposição nula, anulável, inválida ou inoperante não estivesse contida neste termo;
          </p>
          <p>
          O LICENCIADO e USUÁRIOS concordam que a LICENCIANTE pode divulgar a celebração deste instrumento para fins comerciais, fazendo menção ao nome e à marca do LICENCIADO em campanhas comerciais, podendo, inclusive, divulgar mensagens enviadas de forma escrita ou oral, por telefone, para uso em sites, jornais, revistas e outras campanhas, enquanto vigorar o presente termo. O LICENCIADO aceita, ainda, receber comunicações via correio eletrônico sobre treinamentos, parcerias e campanhas relacionadas aos SOFTWARES;
          </p>
          <p>
          Neste ato, a LICENCIANTE expressamente autoriza o LICENCIADO a colher e utilizar seus dados técnicos e operacionais presentes nos SOFTWARES, para fins de estudos e melhorias no SOFTWARE;
          </p>
          <p>
          A LICENCIANTE poderá, a seu exclusivo critério, a qualquer tempo e sem a necessidade de comunicação prévia ao LICENCIADO:
          </p>
          <ol>
            <li>
            Encerrar, modificar ou suspender, total ou parcialmente, o acesso do LICENCIADO ao SOFTWARE, quando referido acesso ou cadastro estiver em violação das condições estabelecidas neste termo;
            </li>
            <li>
            Excluir, total ou parcialmente, as informações cadastradas pelo LICENCIADO que não estejam em consonância com as disposições deste termo;
            </li>
            <li>
            Acrescentar, excluir ou modificar o Conteúdo oferecido no site;
            </li>
            <li>
            Alterar quaisquer termos e condições deste termo mediante comunicação ao LICENCIADO.
            </li>
          </ol>

          <p>
          A LICENCIANTE ainda poderão, a seu exclusivo critério, suspender, modificar ou encerrar as atividades dos SOFTWARES, mediante comunicação prévia ao LICENCIADO, com antecedência mínima de 30 (trinta) dias, disponibilizando formas e alternativas de extrair do Site as informações, salvo nas hipóteses de caso fortuito ou força maior.
          </p>
          <p>
          A LICENCIANTE poderão, por meio de comunicação ao endereço eletrônico ou SMS indicado pelo LICENCIADO em seu cadastro ou por meio de aviso no Site, definir preços para a oferta de determinados conteúdos e/ou serviços, ainda que inicialmente tais serviços tenham sido ofertados de forma gratuita, sendo a utilização destes, após o referido aviso, considerada como concordância do LICENCIADO com a cobrança de tais preços.
          </p>
          <p>
          Fica certo e entendido pelo LICENCIADO que somente a pessoa cadastrada pelo próprio LICENCIADO como administradora de conta poderá solicitar que as informações do LICENCIADO inseridas no Software sejam apagadas. Ainda, o LICENCIADO declara sua ciência de que uma vez apagadas, estas não poderão mais ser recuperadas, ficando a LICENCIANTE isenta de qualquer responsabilidade por quaisquer perdas ou danos decorrentes deste procedimento solicitado pelo LICENCIADO.
          </p>

          <p><strong>18. Da Lei Aplicável</strong></p>
          <p>
          Este presente termo será regido, interpretado e se sujeitará às leis brasileiras e, o LICENCIADO e a LICENCIANTE desde logo elegem, de forma irrevogável e irretratável, o foro da Comarca da Cidade de Jundiaí, Estado de São Paulo, para dirimir quaisquer dúvidas ou controvérsias oriundas deste EULA, com a exclusão de qualquer outro, por mais privilegiado que seja
          </p>

          <p><strong>19. Das definições</strong></p>
          <p>
          <strong>INFORMAÇÕES:</strong> todas informações tramitadas nos SOFTWARES, como por exemplo, dados relativos aos serviços, viaturas, profissionais, usuários, geolocalização, contas, certificados, incluindo logins, senhas e demais informações necessárias para acessar, coletar, armazenar, usar e tratar as informações.
          </p>
          <p>
          <strong>LICENCIADO:</strong> pessoa física ou jurídica, com plena capacidade de contratar, que acessa os SOFTWARES da LICENCIANTE por meio do site, realizando seu cadastro, aceitando os termos do presente EULA e usufruindo das funcionalidades oferecidas de acordo com o plano de licenciamento contratado.
          </p>
          <p>
          <strong>SOFTWARE:</strong> softwares de propriedade exclusiva da LICENCIANTE, cujas funcionalidades e serviços estão disponibilizados pelo site ou aplicativo, por meio do qual as INFORMAÇÕES COMPARTILHADAS do LICENCIADO serão fornecidas diretamente por ele ou coletadas diretamente nos sites das EMPRESAS PARCEIRAS de maneira automatizada.
          </p>
          <p>
          <strong>API:</strong> Application Programming Interface que em português significa Interface de Programação de Aplicativos. É um conjunto de rotinas e padrões de programação para acesso a um aplicativo de software ou plataforma baseado na Web.
          </p>
        </div>
      </div>
    </>
  )
}
