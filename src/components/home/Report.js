import { useState } from 'react';
import styles from '../../styles/components/home/Report.module.css'
import SectionLabel from '../Patterns/SectionLabel'

export default function Report() {

  const reportData = [
    {
      title: "AutEM Mobile",
      text:  "Com o nosso App, seu colaborador terá acesso a todas as informações e processos para realização do atendimento!",
      image: "/images/autem-mobile.png",
    },

    {
      title: "Relatórios",
      text: "Produtividade, diagnósticos e lucro por serviço. Os relatórios são diversos! ",
      image: "/images/autem-relatorios.png",
    },

    {
      title: "Checklist",
      text:  "Personalize por tipo de serviço e empresa, monte o checklist ideal para a sua operação!",
      image: "/images/autem-checklist.png",
    },

    {
      title: "Financeiro",
      text:  "Controle as finanças da sua empresa, fechamento com companhias e remunerações dos colaboradores!",
      image: "/images/autem-financial.png",
    },

    {
      title: "Exportador",
      text:  "Envie em segundos todas as informações de serviços recebidos no portal das seguradoras para a nossa plataforma!",
      image: "/images/autem-exportador.png",
    },

    {
      title: "Comissões",
      text:  "Personalize as tarifas pagas aos colaboradores e confira todas as informações de pagamentos em poucos cliques!",
      image: "/images/autem-comissao.png",
    },

    {
      title: "Logística",
      text:  "Acompanhe em tempo real o status de cada atendimento e visualize checklists e mensagens enviadas pelo seu colaborador!",
      image: "/images/autem-logistico.png",
    },

    {
      title: "Estoque",
      text: "Registre a entrada e saída de produtos e os organize por viatura ou colaborador!",
      image: "/images/autem-estoque.png",
    },

    {
      title: "Integrações",
      text: "Somos integrados com diversas empresas! Compartilhe o sinal dos seus colaboradores através do nosso aplicativo.",
      image: "/images/autem-integracao.png",
    }
  ]

  const arrayLength = reportData.length - 1;

  const [item, setItem] = useState(0);
  const [isActive, setActive] = useState(false);

  function nextItem() {
    if(item < arrayLength) {
      setItem(item + 1);
    } else {
      setItem(0)
    }
    setActive(!isActive);
  }

  function previousItem() {
    if(item > 0) {
      setItem(item - 1);
    } else {
      setItem(arrayLength)
    }
    setActive(!isActive);
  }

  return(
    <div className={styles.report} id="report">
      <div className={styles.container}>
        <section>
          <div data-aos="fade-right" className={styles.reportInfo}>
            <SectionLabel
              text="SOLUÇÕES"
            />
            <div className={isActive ? styles.animate : styles.animateTwo}>
              <h2>
                {reportData[item].title}
              </h2>
              <p>
                {reportData[item].text}
              </p>
            </div>

            <div className={styles.actionButtons}>
              <button
                className={styles.buttonBack}
                onClick={previousItem}
              >
                <img src="/icons/arrow-left.svg"/>
              </button>
              <button
                className={styles.buttonNext}
                onClick={nextItem}
              >
                <img src="/icons/arrow-right.svg"/>
              </button>
            </div>
          </div>
          <div data-aos="fade-left">
            <div className={isActive ? `${styles.imageContainer} ${styles.animateOpacity}` : `${styles.imageContainer} ${styles.animateOpacityTwo}`}>
              <img className={reportData[item].class} src={reportData[item].image} />
            </div>
          </div>
        </section>

        <img className={`${styles.element} ${styles.elementOne}`} src="/elements/dotPatternMin.png" />
        <img className={`${styles.element} ${styles.elementTwo} + cube`} src="/elements/containerHero.png" />
        <img className={`${styles.element} ${styles.elementThree} + cube`} src="/elements/containerHero.png" />
      </div>
    </div>
  );
}