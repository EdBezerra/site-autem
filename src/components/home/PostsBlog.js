import { useRef } from 'react';
import styles from '../../styles/components/home/PostsBlog.module.css';
import SectionLabel from '../Patterns/SectionLabel';
import PostsBlogCard from '../Patterns/PostsBlogCard';

export default function PostsBlog({ data }) {
  const scrollItems = useRef(null);
  return (
    <div className={styles.postblog}>
      <div className={styles.container}>
        <div
          className={styles.items}
          ref={scrollItems}
        >
          {
            data.slice(0, 3).map((item) => {
              return (
                // image, alt, title, dt_pub, text, link,
                <PostsBlogCard
                  key={item.id}
                  image={item.post_img}
                  alt={item.post_title}
                  title={item.post_title}
                  dt_pub={item.post_date}
                  text={item.post_content.substr(0, 200)}
                  link={item.post_url}
                />

              )
            })
          }
        </div>
      </div>
    </div>

  )

}

