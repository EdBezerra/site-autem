import styles from '../styles/components/Footer.module.css';
import { Link } from 'react-scroll';
import { useEffect, useState } from 'react';

export default function Footer() {
  const [path, setPath] = useState();

  const date = new Date;
  const year = date.getFullYear();
  
  useEffect(() => {
    setPath(window.location.pathname)
  });

  return(
    <div className={styles.footer}>
      <div className={styles.container}>
        <section>
          <div className={styles.footerContact}>
            <img src="/images/logo/logo-white.svg" />
            <div>
              <div>
                <img src="icons/phone-alt.svg" />
              </div>
              <a href="tel:551142300152">11 4230-0152</a>
            </div>
            <div>
              <div>
                <img src="icons/mail-alt.svg" />
              </div>
              <a href="mailto:faleconosco@satellitus.com">faleconosco@satellitus.com</a>
            </div>
            <div>
              <div>
                <img src="icons/map-alt.svg" />
              </div>
              <a target="__blank" href="https://g.page/satellitustecnologia?share">Av. Dona Manoela Larcerda de Vergueiro,
                                                                                        58, Anhangabaú, Jundiaí - SP</a>
            </div>
          </div>

          <div className={styles.footerLinks}>
            <h3>
              Links úteis
            </h3>
            <div>
              {
                path == '/terms' ? (
                  <>
                    <a href="/">Home</a>
                    <a href="/">Autem</a>
                    <a href="/">Soluções</a>
                    <Link to="contact" smooth={true} duration={1000}>Contato</Link>
                  </>
                ) : (
                  <>
                    <Link to="home" smooth={true} duration={1000}>Home</Link>
                    <Link to="autem" smooth={true} duration={1000}>Autem</Link>
                    <Link to="report" smooth={true} duration={1000}>Soluções</Link>
                    <Link to="contact" smooth={true} duration={1000}>Contato</Link>
                  </>
                )
              }
              <a href="terms">Termos de uso</a>
              <a href="https://web.autem.com.br/login/">Login</a>
            </div>
          </div>

          <div className={styles.footerSocial}>
            <h3>Redes sociais</h3>
            <div>
              <a href="https://www.instagram.com/satellitustecnologia/">Ig</a>
              <a href="https://www.facebook.com/satellitustecnologia/">Fb</a>
              <a href="https://www.linkedin.com/company/satellitus/mycompany/">In</a>

              <a href="https://api.whatsapp.com/send?phone=5511982241848&text=Olá, gostaria de falar com um especialista no AutEM." 
                    className={`${styles.whatsappButton}`} 
                    target="_blank" >
               <img src="https://i.ibb.co/VgSspjY/whatsapp-button.png" alt="botão whatsapp" />
              </a>

            </div>
          </div>
        </section>
        <span>Powered by Satellitus &copy; {year} </span>

        <img className={`${styles.element} + ${styles.elementOne} + cube`} src="/elements/dotPatternMin.png"/>
        <img className={`${styles.element} + ${styles.elementTwo} + cube`} src="/elements/dotPatternAlt.png"/>
      </div>
    </div>
  );
}