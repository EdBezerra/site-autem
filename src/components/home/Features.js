import styles from '../../styles/components/home/Features.module.css'
import MiniCard from '../Patterns/MiniCard'

export default function Features() {
  return(
    <div className={styles.container}>
      <section data-aos="fade-up">
        <MiniCard
          title="Acionamento"
          text="Serviços enviados em questão de segundos."
          icon="/icons/access.svg"
        />
        <MiniCard
          title="Monitoramento"
          text="Visualize sua equipe em tempo real através do App."
          icon="/icons/eye.svg"
        />
        <MiniCard
          title="Mensagens"
          text="Comunicação direta entre sua empresa e colaborador!"
          icon="/icons/message.svg"
        />
        <MiniCard
          title="Checklist"
          text="Personalize suas perguntas e envie pelo AutEM Mobile."
          icon="/icons/checklist.svg"
        />
        <MiniCard
          title="Tempo médio"
          text="Analise o tempo de chegada e execução de serviços."
          icon="/icons/timer.svg"
        />
        <MiniCard
          title="Gerenciamento"
          text="Relatórios completos para análise de desempenho."
          icon="/icons/graphic.svg"
        />
        <MiniCard
          title="Exportador"
          text="Envie serviços de portais de seguradoras em um clique."
          icon="/icons/captation.svg"
        />
        <MiniCard
          title="Financeiro"
          text="Controle todo o fluxo de caixa da sua empresa."
          icon="/icons/financial.svg"
        />
      </section>

        <img className={`${styles.element} + ${styles.elementOne} + cube`} src="/elements/dotPattern.png"/>
        <img className={`${styles.element} + ${styles.elementTwo} + cube`} src="/elements/dotPatternAlt.png"/>
        <img className={`${styles.element} + ${styles.elementThree} + cube`} src="/elements/containerHero.png"/>
    </div>
  );
}