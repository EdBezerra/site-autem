import { createContext, useContext, useState } from 'react';
import ModalTrigger from '../components/home/ModalTrigger';
import Mobile from '../components/home/Mobile';
import Trial from '../components/home/Trial';

export const ModalTriggerContext = createContext({});

export function ModalTriggerProvider({ children, ...rest }) {

  const [isModalTrigger, setIsModalTrigger] = useState(false);
  const [modalStatus, setModalStatus] = useState(false);
  const [sendService, setSendService] = useState(false);
  const [openMobile, setOpenMobile] = useState(false);
  const [buttonAccept, setButtonAccept] = useState(false);
  const [isClicked, setIsClicked] = useState(false);

  //For show Sat, set this state as true
  const [sat, setSat] = useState(false);

  function closeMessage() {
    setSat(false);
  }
  
  function openModalTrigger() {
    setIsModalTrigger(true);
    closeMessage()
  }
  
  function closeModalTrigger() {
    setIsModalTrigger(false);
  }

  function goSat() {
    setModalStatus(true);
    setSendService(true);
    setButtonAccept(true);

    setTimeout(() => {
      closeModalTrigger();
      setModalStatus(false);
    }, 300);
  }

  function openImmersion() {
    setSendService(false);
    setOpenMobile(true);
  }

  function closeNotification() {
    setSendService(false);
  }

  function closeImmersion() {
    setOpenMobile(false);
  }

  return (
    <ModalTriggerContext.Provider
      value={{
        isModalTrigger,
        modalStatus,
        sendService,
        openMobile,
        buttonAccept,
        isClicked,
        sat,
        closeMessage,
        setIsClicked,
        closeImmersion,
        setButtonAccept,
        setOpenMobile,
        setIsModalTrigger,
        setModalStatus,
        setSendService,
        openModalTrigger,
        closeModalTrigger,
        goSat,
        openImmersion,
        closeNotification
      }}
    >
      {children}
      { isModalTrigger && <ModalTrigger /> }
      { openMobile && <Mobile /> }
    </ModalTriggerContext.Provider>
  )
}
